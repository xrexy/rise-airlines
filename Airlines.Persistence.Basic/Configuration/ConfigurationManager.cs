using Microsoft.Extensions.Configuration;

namespace Airlines.Persistence.Basic.Configuration;

internal static class ConfigurationManager
{
    private static readonly IConfigurationRoot _configuration;

    static ConfigurationManager()
    {
        _configuration = new ConfigurationBuilder()
            .AddJsonFile("appsettings.json")
            .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")}.json", optional: true)
            .Build();
    }

    internal static string? GetConnectionString(string path)
      => _configuration.GetConnectionString(path);
}
