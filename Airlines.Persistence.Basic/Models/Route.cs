namespace Airlines.Persistence.Basic.Models;

public class Route(string identifier) : IModel
{
    public string Identifier => identifier.ToUpper();

    public LinkedList<Flight> Flights { get; } = new();

    /// <summary>
    /// Adds a flight to the route
    /// </summary>
    /// <param name="flight">The flight to add</param>
    /// <exception cref="InvalidOperationException">Thrown if the flight is not logically connected</exception>
    public void AddFlight(Flight flight)
    {
        var lastFlight = Flights.LastOrDefault();
        if (lastFlight != null && !this.AreLogicallyConnected(lastFlight, flight))
        {
            throw new InvalidOperationException("Flight is not logically connected");
        }

        _ = Flights.AddLast(flight);
    }

    /// <summary>
    /// Removes the last flight
    /// </summary>
    public void RemoveLastFlight()
      => Flights.RemoveLast();

    /// <summary>
    /// Checks if two flights are logically connected. This means that the arrival and departure airports are the same
    /// </summary>
    /// <returns><c>true</c>, if logically connected, <c>false</c> otherwise.</returns>
    /// <param name="f1">The first flight</param>
    /// <param name="f2">The second flight</param>
    public bool AreLogicallyConnected(Flight f1, Flight f2)
      // =>  arrivalFlight.ArrivalAirport == departureFlight.DepartureAirport;
      => string.Equals(f1.ArrivalAirport, f2.DepartureAirport, StringComparison.OrdinalIgnoreCase);
}
