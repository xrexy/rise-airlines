namespace Airlines.Persistence.Basic.Models.Baggage;

public static class LargeBaggage
{
    public static uint MaximumWeight => 30;
    public static double MaximumVolume => 0.090;
}
