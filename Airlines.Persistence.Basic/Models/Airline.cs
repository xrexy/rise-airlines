namespace Airlines.Persistence.Basic.Models;

public class Airline(string identifier, string name) : IModel
{
    public string Identifier => identifier.ToUpper();
    public string Name => name;
}
