using Airlines.Persistence.Basic.Models.AircraftTypes;

namespace Airlines.Persistence.Basic.Models;

public class Aircraft(string modelName, IAircraftType type) : IModel
{
    public string ModelName => NormalizeModelName(modelName);
    public IAircraftType Type => type;

    public static string NormalizeModelName(string modelName)
        => modelName.ToUpper().Replace(" ", "");
}
