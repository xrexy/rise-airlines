namespace Airlines.Persistence.Basic.Models.AircraftTypes;

public class PassengerAircraftType(uint seats, uint cargoCapacity, double cargoWeight) : ITicketableAircraftType, ICargoAircraftType
{
    public uint Seats => seats;
    public uint CargoWeight => cargoCapacity;
    public double CargoVolume => cargoWeight;
}
