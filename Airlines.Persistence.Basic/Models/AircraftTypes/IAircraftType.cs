namespace Airlines.Persistence.Basic.Models.AircraftTypes;

public interface IAircraftType;
public interface ITicketableAircraftType : IAircraftType
{
    public uint Seats { get; }
}

public interface ICargoAircraftType : IAircraftType
{
    public uint CargoWeight { get; }
    public double CargoVolume { get; }
}
