﻿namespace Airlines.Persistence.Basic.Entities;

public partial class Airport
{
    public int Id { get; set; }

    public string Code { get; set; } = null!;

    public string Name { get; set; } = null!;

    public string Country { get; set; } = null!;

    public string City { get; set; } = null!;

    public int Runways { get; set; }

    public int Founded { get; set; }

    public virtual ICollection<Flight> FlightFromAirports { get; set; } = new List<Flight>();

    public virtual ICollection<Flight> FlightToAirports { get; set; } = new List<Flight>();

    public override string ToString() => $"[Airport] Id: {Id}, Name: {Name}, Code: {Code}, Country: {Country}, City: {City}, Runways: {Runways}, Founded: {Founded}";
}
