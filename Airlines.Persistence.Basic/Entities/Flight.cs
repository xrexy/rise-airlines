﻿namespace Airlines.Persistence.Basic.Entities;

public partial class Flight
{
    public int Id { get; set; }

    public string FlightNumber { get; set; } = null!;

    public int AirlineId { get; set; }

    public int FromAirportId { get; set; }

    public int ToAirportId { get; set; }

    public DateTime DepartureDateTime { get; set; }

    public DateTime ArrivalDateTime { get; set; }

    public virtual Airline Airline { get; set; } = null!;

    public virtual Airport FromAirport { get; set; } = null!;

    public virtual Airport ToAirport { get; set; } = null!;
}
