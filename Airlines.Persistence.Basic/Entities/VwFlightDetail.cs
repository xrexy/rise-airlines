﻿namespace Airlines.Persistence.Basic.Entities;

public partial class VwFlightDetail
{
    public int FlightId { get; set; }

    public string FlightNumber { get; set; } = null!;

    public DateTime DepartureDateTime { get; set; }

    public DateTime ArrivalDateTime { get; set; }

    public int AirlineId { get; set; }

    public string AirlineCode { get; set; } = null!;

    public string Airline { get; set; } = null!;

    public string AirlineDescription { get; set; } = null!;

    public int AirlineFleetSize { get; set; }

    public int AirlineFounded { get; set; }

    public int AirportId { get; set; }

    public string AirportCode { get; set; } = null!;

    public string Airport { get; set; } = null!;

    public string AirportCity { get; set; } = null!;

    public string AirportCountry { get; set; } = null!;

    public int AirportRunways { get; set; }

    public int AirportFounded { get; set; }
}
