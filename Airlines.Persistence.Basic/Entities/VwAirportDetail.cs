﻿namespace Airlines.Persistence.Basic.Entities;

public partial class VwAirportDetail
{
    public int AirportId { get; set; }

    public string AirportCode { get; set; } = null!;

    public string Airport { get; set; } = null!;

    public string AirportCity { get; set; } = null!;

    public string AirportCountry { get; set; } = null!;

    public int AirportRunways { get; set; }

    public int AirportFounded { get; set; }
}
