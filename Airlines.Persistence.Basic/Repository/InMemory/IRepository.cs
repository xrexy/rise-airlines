namespace Airlines.Persistence.Basic.Repository.InMemory;

public interface IRepository<T> where T : class
{
    public T? GetById(string id);

    public IEnumerable<T> GetAll();

    public int Size { get; }

    public bool Add(T entity);

    public bool Update(T entity);

    public bool Delete(string id);

    public bool Delete(T entity);

    public bool Exists(string id);

    public bool Exists(T entity);

    public void DeleteAll();
}
