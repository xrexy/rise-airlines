using Airlines.Persistence.Basic.Models;

namespace Airlines.Persistence.Basic.Repository.InMemory;

[Obsolete("This is an in-memory repository. A DB based one should be used instead.")]
public class AirlineRepository : IRepository<Airline>
{
    private Dictionary<string, Airline> Airlines { get; } = [];

    public int Size => Airlines.Count;

    public bool Add(Airline entity)
    {
        if (this.Exists(entity))
        {
            return false;
        }

        this.Set(entity);
        return true;
    }

    public bool Delete(string id)
        => Airlines.Remove(id.ToUpper());

    public bool Delete(Airline entity)
        => this.Delete(entity.Identifier);

    public void DeleteAll()
        => Airlines.Clear();

    public bool Exists(string id)
        => Airlines.ContainsKey(id.ToUpper());

    public bool Exists(Airline entity)
        => this.Exists(entity.Identifier);

    public Airline? GetById(string id)
        => Airlines.GetValueOrDefault(id.ToUpper());

    public IEnumerable<Airline> GetAll() => Airlines.Values;

    public bool Update(Airline entity)
    {
        if (!this.Exists(entity))
        {
            return false;
        }

        this.Set(entity);
        return true;
    }

    private void Set(Airline entity)
        => Airlines[entity.Identifier.ToUpper()] = entity;
}
