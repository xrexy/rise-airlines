using Airlines.Persistence.Basic.Entities;
using Airlines.Persistence.Basic.Exceptions;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.Linq.Expressions;
using DbContext = Airlines.Persistence.Basic.Entities.DbContext;

namespace Airlines.Persistence.Basic.Repository;

public interface IFlightRepository : IRepository<Flight>;

public class FlightRepository : DbContextProvider, IFlightRepository
{
    public Task<List<Flight>> GetAllAsync()
        => this.RunAction(ctx => ctx.Flights
            .Include(x => x.FromAirport)
            .Include(x => x.ToAirport)
            .Include(x => x.Airline)
            .ToListAsync(), []);

    public Task<Flight?> GetByIdAsync(int id)
        => this.RunAction(async _ =>
        {
            var all = await this.GetAll((x) => x.Id == id);
            return all.FirstOrDefault();
        });

    public Task<List<Flight>> GetAll(Expression<Func<Flight, bool>> predicate)
        => this.RunAction(ctx => ctx.Flights
                .Where(predicate).Include(x => x.FromAirport)
                .Include(x => x.ToAirport)
                .Include(x => x.Airline)
                .ToListAsync(),
                []
            );

    public Task<Flight?> GetSingle(Expression<Func<Flight, bool>> predicate)
        => this.RunAction(ctx => ctx.Flights.SingleAsync(predicate));

    public Task<int> AddAsync(Flight entity)
        => this.RunAction(async ctx =>
        {
            this.PreprocessEntity(entity);

            var addRes = ctx.Flights.Add(entity);
            var isSuccess = await ctx.SaveChangesAsync() > 0;
            return isSuccess ? addRes.Entity.Id : -1;
        }, -1);

    public Task<bool> DeleteAsync(int id)
        => this.RunAction(async _ =>
        {
            var flight = await this.GetByIdAsync(id);
            return flight != null && await this.DeleteAsync(flight);
        });

    public Task<bool> DeleteAsync(Flight entity)
        => this.RunAction(async ctx =>
        {
            _ = ctx.Flights.Remove(entity);
            return await Context.SaveChangesAsync() > 0;
        });

    public Task<bool> ExistsAsync(int id)
        => this.RunAction(_ => this.ExistsPredicateAsync((x) => x.Id == id));

    public Task<bool> ExistsAsync(Flight entity)
        => this.ExistsAsync(entity.Id);

    public Task<bool> ExistsPredicateAsync(Expression<Func<Flight, bool>> predicate)
        => this.RunAction(ctx => ctx.Flights.AnyAsync(predicate));

    public Task<bool> UpdateAsync(Flight entity)
        => this.RunAction(async ctx =>
        {
            _ = ctx.Flights.Update(entity);
            return await ctx.SaveChangesAsync() > 0;
        });

    public void PreprocessEntity(Flight entity)
    {
        if (entity.ArrivalDateTime < entity.DepartureDateTime)
        {
            throw new InvalidEntityException("Invalid flight entity; ArrivalDateTime < DepartureDateTime");
        }

        if (entity.ArrivalDateTime == entity.DepartureDateTime)
        {
            throw new InvalidEntityException("Invalid flight entity; ArrivalDateTime == DepartureDateTime");
        }

        if (entity.ToAirportId == entity.FromAirportId)
        {
            throw new InvalidEntityException("Invalid flight entity; ToAirportId == FromAirportId");
        }

        if (entity.FlightNumber.IsNullOrEmpty())
        {
            throw new InvalidEntityException("Invalid flight entity; FlightNumber is null");
        }

        entity.FlightNumber = entity.FlightNumber.ToUpper();
    }

    private async Task<T> RunAction<T>(Func<DbContext, Task<T>> action, T defaultValue)
    {
        try
        {
            return await action(Context);
        }
        catch (Exception e)
        {
            Console.WriteLine(RepositoryHelpers.GetSimplifiedExceptionDebugInfo(e, "AirlineRepository#" + action.Method.Name));
            return defaultValue;
        }
    }

    private async Task<T?> RunAction<T>(Func<DbContext, Task<T>> action)
    {
        try
        {
            return await action(Context);
        }
        catch (Exception e)
        {
            Console.WriteLine(RepositoryHelpers.GetSimplifiedExceptionDebugInfo(e, "AirlineRepository#" + action.Method.Name));
            return default;
        }
    }
}
