using Airlines.Persistence.Basic.Models;

namespace Airlines.Persistence.Basic.Repository;

public class RouteRepository : InMemory.IRepository<Route>
{
    private Dictionary<string, Route> Routes { get; } = [];

    public int Size => Routes.Count;

    public bool Add(Route entity)
    {
        if (this.Exists(entity))
        {
            return false;
        }

        this.Set(entity);
        return true;
    }

    public bool Delete(string id)
        => Routes.Remove(id.ToUpper());

    public bool Delete(Route entity)
        => this.Delete(entity.Identifier);

    public void DeleteAll()
        => Routes.Clear();

    public bool Exists(string id)
        => Routes.ContainsKey(id.ToUpper());

    public bool Exists(Route entity)
        => this.Exists(entity.Identifier);

    public Route? GetById(string id)
        => Routes.GetValueOrDefault(id.ToUpper());

    public IEnumerable<Route> GetAll() => Routes.Values;

    public bool Update(Route entity)
    {
        if (!this.Exists(entity))
        {
            return false;
        }

        this.Set(entity);
        return true;
    }

    private void Set(Route entity)
        => Routes[entity.Identifier.ToUpper()] = entity;
}
