using Airlines.Persistence.Basic.Entities;

namespace Airlines.Persistence.Basic.Repository;

public abstract class DbContextProvider : IDisposable
{
    public DbContext Context { get; internal set; } = null!;

    // we want to set the dbcontext only if it's not been overwritten
    public DbContextProvider() => Context ??= new DbContext(DbContext.ConnectionType.SqlServer);

    public void Dispose()=> Context?.Dispose();
}
