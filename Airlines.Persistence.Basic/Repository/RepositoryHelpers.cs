using Airlines.Persistence.Basic.Exceptions;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace Airlines.Persistence.Basic.Repository;

public static class RepositoryHelpers
{
    public static string GetSimplifiedExceptionDebugInfo(Exception e, string? prefix = null)
    {
        prefix ??= e.GetType().Name;
        var content = e switch
        {
            InvalidEntityException invalidEntityException => $"Invalid Entity: {invalidEntityException.Message}",
            SqlException sqlException => $"SQL Error: {sqlException.Message}",
            DbUpdateConcurrencyException dbUpdateConcurrencyException => $"Concurrent Entity Error: {dbUpdateConcurrencyException.Message}",
            _ => $"Unknown Error: \n{e}"
        };

        return $"[{prefix}] {content}";
    }
}
