
using Airlines.Persistence.Basic.Models;

namespace Airlines.Persistence.Basic.Repository;

public class AircraftRepository : InMemory.IRepository<Aircraft>
{
    private Dictionary<string, Aircraft> Aircrafts { get; } = [];

    public int Size => Aircrafts.Count;

    public bool Add(Aircraft entity)
    {
        if (this.Exists(entity))
        {
            return false;
        }

        this.Set(entity);
        return true;
    }

    public bool Delete(string id)
        => Aircrafts.Remove(id.ToUpper());

    public bool Delete(Aircraft entity)
        => this.Delete(entity.ModelName);

    public void DeleteAll()
        => Aircrafts.Clear();

    public bool Exists(string id)
        => Aircrafts.ContainsKey(Aircraft.NormalizeModelName(id));

    public bool Exists(Aircraft entity)
        => this.Exists(entity.ModelName);

    public Aircraft? GetById(string id)
        => Aircrafts.GetValueOrDefault(id.ToUpper());

    // can't be null since it's validated when creating the flight
    public Aircraft GetByFlight(Flight flight)
        => this.GetById(flight.AircraftModel)!;

    public IEnumerable<Aircraft> GetAll() => Aircrafts.Values;

    public bool Update(Aircraft entity)
    {
        if (!this.Exists(entity))
        {
            return false;
        }

        this.Set(entity);
        return true;
    }

    private void Set(Aircraft entity)
        => Aircrafts[entity.ModelName] = entity;
}
