using Airlines.Persistence.Basic.Entities;
using Airlines.Persistence.Basic.Exceptions;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.Linq.Expressions;
using DbContext = Airlines.Persistence.Basic.Entities.DbContext;

namespace Airlines.Persistence.Basic.Repository;

public interface IAirlineRepository : IRepository<Airline>;

public class AirlineRepository : DbContextProvider, IAirlineRepository
{
    public Task<List<Airline>> GetAllAsync()
        => this.RunAction(ctx => ctx.Airlines.ToListAsync(), []);

    public Task<Airline?> GetByIdAsync(int id)
        => this.RunAction(ctx => ctx.Airlines.FirstOrDefaultAsync(x => x.Id == id), null);

    public Task<List<Airline>> GetAll(Expression<Func<Airline, bool>> predicate)
        => this.RunAction(ctx => ctx.Airlines.Where(predicate).ToListAsync(), []);

    public Task<Airline?> GetSingle(Expression<Func<Airline, bool>> predicate)
        => this.RunAction(ctx => ctx.Airlines.SingleAsync(predicate));

    public Task<int> AddAsync(Airline entity)
    => this.RunAction(async ctx =>
    {
        this.PreprocessEntity(entity);

        var addRes = ctx.Airlines.Add(entity);
        var isSuccess = await ctx.SaveChangesAsync() > 0;
        return isSuccess ? addRes.Entity.Id : -1;
    }, -1);

    public Task<bool> DeleteAsync(int id)
        => this.RunAction(async _ =>
        {
            var airline = await this.GetByIdAsync(id);
            return airline != null && await this.DeleteAsync(airline);
        });

    public Task<bool> DeleteAsync(Airline entity)
        => this.RunAction(async ctx =>
        {
            _ = ctx.Airlines.Remove(entity);
            return await ctx.SaveChangesAsync() > 0;
        });

    public Task<bool> ExistsAsync(int id)
        => this.RunAction(_ => this.ExistsPredicateAsync((x) => x.Id == id));

    public Task<bool> ExistsPredicateAsync(Expression<Func<Airline, bool>> predicate)
        => this.RunAction(ctx => ctx.Airlines.AnyAsync(predicate));

    public Task<bool> ExistsAsync(Airline entity)
        => this.ExistsAsync(entity.Id);

    public Task<bool> UpdateAsync(Airline entity)
        => this.RunAction(async ctx =>
        {
            var entry = ctx.Entry(entity);
            if (entry.State == EntityState.Detached)
            {
                _ = ctx.Airlines.Attach(entity);
            }

            var res = ctx.Airlines.Update(entity);
            return await ctx.SaveChangesAsync() > 0;
        });

    public void PreprocessEntity(Airline entity)
    {
        if (entity.FleetSize < 1)
        {
            throw new InvalidEntityException("Invalid airline entity; FleetSize must be at least 1");
        }

        if (entity.Name.IsNullOrEmpty())
        {
            throw new InvalidEntityException("Invalid airline entity; Name is null");
        }

        if (entity.Name.Length > 128)
        {
            throw new InvalidEntityException("Invalid airline entity; Name length larger than 128");
        }

        if (entity.Founded < 1900)
        {
            throw new InvalidEntityException("Invalid airline entity; Founded < 1900");
        }

        var currentYear = DateTime.Now.Year;
        if (entity.Founded > currentYear)
        {
            throw new InvalidEntityException($"Invalid airline entity; Founded > Current Year({currentYear})");
        }

        if (entity.Code.IsNullOrEmpty())
        {
            throw new InvalidEntityException("Invalid airline entity; Code is null");
        }

        if (entity.Code.Length != 2)
        {
            throw new InvalidEntityException("Invalid airline entity; Code must be 2 characters");
        }

        if (entity.Description.IsNullOrEmpty())
        {
            entity.Description = "No description provided";
        }

        if (entity.Description.Length > 256)
        {
            throw new InvalidEntityException("Invalid airline entity; Description length larger than 256");
        }

        entity.Code = entity.Code.ToUpper();
    }

    private async Task<T> RunAction<T>(Func<DbContext, Task<T>> action, T defaultValue)
    {
        try
        {
            return await action(Context);
        }
        catch (Exception e)
        {
            Console.WriteLine(RepositoryHelpers.GetSimplifiedExceptionDebugInfo(e, "AirlineRepository#" + action.Method.Name));
            return defaultValue;
        }
    }

    private async Task<T?> RunAction<T>(Func<DbContext, Task<T>> action)
    {
        try
        {
            return await action(Context);
        }
        catch (Exception e)
        {
            Console.WriteLine(RepositoryHelpers.GetSimplifiedExceptionDebugInfo(e, "AirlineRepository#" + action.Method.Name));
            return default;
        }
    }
}
