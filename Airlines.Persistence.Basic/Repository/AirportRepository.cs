
using Airlines.Persistence.Basic.Entities;
using Airlines.Persistence.Basic.Exceptions;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.Linq.Expressions;
using DbContext = Airlines.Persistence.Basic.Entities.DbContext;

[assembly: System.Runtime.CompilerServices.InternalsVisibleTo("Airlines.Tests")]
namespace Airlines.Persistence.Basic.Repository;

public interface IAirportRepository : IRepository<Airport>;

public class AirportRepository : DbContextProvider, IAirportRepository
{
    public Task<List<Airport>> GetAllAsync()
        => this.RunAction(ctx => ctx.Airports.ToListAsync(), []);

    public Task<Airport?> GetByIdAsync(int id)
        => this.RunAction(ctx => ctx.Airports.FirstOrDefaultAsync(x => x.Id == id), null);

    public Task<List<Airport>> GetAll(Expression<Func<Airport, bool>> predicate)
        => this.RunAction(ctx => ctx.Airports.Where(predicate).ToListAsync(), []);

    public Task<Airport?> GetSingle(Expression<Func<Airport, bool>> predicate)
        => this.RunAction(ctx => ctx.Airports.SingleAsync(predicate));

    public Task<int> AddAsync(Airport entity)
        => this.RunAction(async ctx =>
        {
            this.PreprocessEntity(entity);

            var addRes = ctx.Airports.Add(entity);
            var isSuccess = await ctx.SaveChangesAsync() > 0;
            return isSuccess ? addRes.Entity.Id : -1;
        }, -1);

    public Task<bool> DeleteAsync(int id)
        => this.RunAction(async _ =>
        {
            var airport = await this.GetByIdAsync(id);
            return airport != null && await this.DeleteAsync(airport);
        });

    public Task<bool> DeleteAsync(Airport entity)
        => this.RunAction(async ctx =>
        {
            _ = ctx.Airports.Remove(entity);
            return await ctx.SaveChangesAsync() > 0;
        });

    public Task<bool> ExistsAsync(int id)
        => this.RunAction(_ => this.ExistsPredicateAsync((x) => x.Id == id));

    public Task<bool> ExistsPredicateAsync(Expression<Func<Airport, bool>> predicate)
        => this.RunAction(ctx => ctx.Airports.AnyAsync(predicate));

    public Task<bool> ExistsAsync(Airport entity)
        => this.ExistsAsync(entity.Id);

    public Task<bool> UpdateAsync(Airport entity)
        => this.RunAction(async ctx =>
        {
            var entry = ctx.Entry(entity);
            if (entry.State == EntityState.Detached)
            {
                _ = ctx.Airports.Attach(entity);
            }

            var res = ctx.Airports.Update(entity);
            return await ctx.SaveChangesAsync() > 0;
        });

    public void PreprocessEntity(Airport entity)
    {
        if (entity.Founded < 1900)
        {
            throw new InvalidEntityException("Invalid airport entity; Founded < 1900");
        }

        var currentYear = DateTime.Now.Year;
        if (entity.Founded > currentYear)
        {
            throw new InvalidEntityException($"Invalid airport entity; Founded > Current Year({currentYear})");
        }

        if (entity.Runways is < 1)
        {
            throw new InvalidEntityException("Invalid airport entity; Runways < 1");
        }

        if (entity.Name.IsNullOrEmpty())
        {
            throw new InvalidEntityException("Invalid airport entity; Name is null");
        }

        if (entity.Country.IsNullOrEmpty())
        {
            throw new InvalidEntityException("Invalid airport entity; Country is null");
        }

        if (entity.City.IsNullOrEmpty())
        {
            throw new InvalidEntityException("Invalid airport entity; City is null");
        }

        if (entity.Code.IsNullOrEmpty())
        {
            throw new InvalidEntityException("Invalid airport entity; Code is null");
        }

        if (entity.Code.Length is > 3 or < 1)
        {
            throw new InvalidEntityException("Invalid airport entity; Code is an invalid length (1-3)");
        }

        entity.FlightFromAirports ??= [];
        entity.FlightToAirports ??= [];
        entity.Code = entity.Code.ToUpper();
    }

    private async Task<T> RunAction<T>(Func<DbContext, Task<T>> action, T defaultValue)
    {
        try
        {
            return await action(Context);
        }
        catch (Exception e)
        {
            Console.WriteLine(RepositoryHelpers.GetSimplifiedExceptionDebugInfo(e, "AirportRepository#" + action.Method.Name));
            return defaultValue;
        }
    }

    private async Task<T?> RunAction<T>(Func<DbContext, Task<T>> action)
    {
        try
        {
            return await action(Context);
        }
        catch (Exception e)
        {
            Console.WriteLine(RepositoryHelpers.GetSimplifiedExceptionDebugInfo(e, "AirportRepository#" + action.Method.Name));
            return default;
        }
    }
}
