namespace Airlines.Persistence.Basic.Exceptions;

public class InvalidEntityException : Exception
{
    public InvalidEntityException() : base("Invalid Entity") {}
    public InvalidEntityException(string? message) : base(message) {}
    public InvalidEntityException(string? message, Exception? innerException) : base(message, innerException) {}
}
