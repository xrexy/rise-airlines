-- Create a new view called 'vw_AirlineView'

USE NemetschekConsoleApp;
GO

DROP VIEW IF EXISTS vw_AirlineDetails;
GO

CREATE VIEW vw_AirlineDetails
AS
  SELECT
    Id as AirlineId,
    Code as AirlineCode,
    Name as Airline,
    Description as AirlineDescription,
    FleetSize as AirlineFleetSize,
    Founded as AirlineFounded
  FROM dbo.Airlines
GO
