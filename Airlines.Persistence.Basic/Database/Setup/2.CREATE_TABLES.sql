USE NemetschekConsoleApp;
GO

-- Create a new table called 'Airports' in schema 'dbo'
CREATE TABLE dbo.Airports
(
  Id INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
  Code NVARCHAR(3) NOT NULL UNIQUE,
  -- IATA code
  Name NVARCHAR(128) NOT NULL,
  Country NVARCHAR(128) NOT NULL,
  City NVARCHAR(128) NOT NULL,
  Runways INT NOT NULL,
  Founded INT NOT NULL
);
GO

-- Create a new table called 'Airlines' in schema 'dbo'
CREATE TABLE dbo.Airlines
(
  Id INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
  Code NVARCHAR(2) NOT NULL UNIQUE,
  -- IATA code
  Name NVARCHAR(128) NOT NULL,
  Description NVARCHAR(256) NOT NULL,
  FleetSize INT NOT NULL,
  Founded INT NOT NULL,
);
GO

-- Create a new table called 'Flights' in schema 'dbo'
CREATE TABLE dbo.Flights
(
  Id INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
  FlightNumber NVARCHAR(10) NOT NULL UNIQUE,
  AirlineId INT NOT NULL,
  FromAirportId INT NOT NULL,
  ToAirportId INT NOT NULL,
  DepartureDateTime DATETIME NOT NULL,
  ArrivalDateTime DATETIME NOT NULL,

  -- Add all foreign key constraints
  CONSTRAINT FK_Airline FOREIGN KEY (AirlineId) REFERENCES Airlines(Id),
  CONSTRAINT FK_FromAirport FOREIGN KEY (FromAirportId) REFERENCES Airports(Id),
  CONSTRAINT FK_ToAirport FOREIGN KEY (ToAirportId) REFERENCES Airports(Id),

  -- Add all check constraints(make sure times are correct)
  CONSTRAINT CHK_DepartureIsBeforeArrival CHECK (DepartureDateTime < ArrivalDateTime),
  CONSTRAINT CHK_DepartureIsAfterNow CHECK (DepartureDateTime > GETDATE()),
);
GO

-- Create a nonclustered index on the 'DepartureDateTime' column of the 'Flights' table
CREATE NONCLUSTERED INDEX IDX_FlightsDeparture 
  ON dbo.Flights(DepartureDateTime);
GO

-- Create a nonclustered index on the 'ArrivalDateTime' column of the 'Flights' table
CREATE NONCLUSTERED INDEX IDX_FlightsArrival 
  ON dbo.Flights(ArrivalDateTime);
GO

-- Create a nonclustered index on the 'FromAirportId' column of the 'Flights' table
CREATE NONCLUSTERED INDEX IDX_FlightsFromAirport 
  ON dbo.Flights(FromAirportId);
GO

-- Create a nonclustered index on the 'ToAirportId' column of the 'Flights' table
CREATE NONCLUSTERED INDEX IDX_FlightsToAirport 
  ON dbo.Flights(ToAirportId);
GO
