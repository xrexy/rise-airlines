-- Create a new view called 'vw_AirportDetails'

USE NemetschekConsoleApp;
GO

DROP VIEW IF EXISTS vw_AirportDetails;
GO

CREATE VIEW vw_AirportDetails
AS
  SELECT
    Id as AirportId,
    Code as AirportCode,
    Name as Airport,
    City as AirportCity,
    Country as AirportCountry,
    Runways as AirportRunways,
    Founded as AirportFounded
  FROM dbo.Airports
GO
