USE NemetschekConsoleApp;
GO

-- Add airports
INSERT INTO dbo.Airports
  (Code, Name, Country, City, Runways, Founded)
VALUES
  ('DFW', 'Dallas', 'USA', 'Dallas', 4, 1987),
  ('LAX', 'Los Angeles', 'USA', 'los angeles', 4, 1987),
  ('DCA', 'Washington', 'USA', 'washington', 4, 1987),
  ('SOF', 'Sofia', 'Bulgaria', 'sofia', 4, 1987);
GO

-- Add airlines
INSERT INTO dbo.Airlines
  (Code, Name, Description, FleetSize, Founded)
VALUES
  ('AA', 'American', 'American Airlines', 100, 1987),
  ('IB', 'Iberia', 'Iberia', 100, 1987);
GO

-- Insert rows into table 'Flights'
INSERT INTO dbo.Flights
  (FlightNumber, AirlineId, FromAirportId, ToAirportId, DepartureDateTime, ArrivalDateTime)
VALUES
  ('FL123', 1, 1, 2, '2028-04-02 07:00:00', '2028-04-02 11:00:00'),
  ('FL101', 1, 3, 4, '2028-04-08 01:00:00', '2028-04-08 03:30:00');
GO

SELECT FlightNumber FROM dbo.Flights AS f
WHERE FlightNumber = 'FL123'
GO

UPDATE dbo.Flights
SET ToAirportId = 3
WHERE FlightNumber = 'FL123';
GO

DELETE FROM dbo.Flights
-- WHERE FlightNumber = 'FL101';
WHERE Id = 2;
GO

SELECT f.FlightNumber FROM dbo.Flights AS f
WHERE Id = 0;
GO
