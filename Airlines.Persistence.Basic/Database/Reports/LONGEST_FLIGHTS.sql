-- Show the longest flight (depends on arrival and departure time) with all possible details for Airports, Airlines and Flight
USE NemetschekConsoleApp;
GO

SELECT TOP 1
  DATEDIFF(MINUTE, fl.DepartureDateTime, fl.ArrivalDateTime) as FlightDurationMinutes,
  fl.*
FROM dbo.vw_FlightDetails fl
ORDER BY fl.DepartureDateTime DESC
