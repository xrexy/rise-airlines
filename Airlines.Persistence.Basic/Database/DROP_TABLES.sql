USE NemetschekConsoleApp;

DROP TABLE IF EXISTS dbo.Flights;
DROP TABLE IF EXISTS dbo.Airlines;
DROP TABLE IF EXISTS dbo.Airports;
