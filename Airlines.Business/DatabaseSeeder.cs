using Airlines.Business.Input;
using Airlines.Persistence.Basic.Entities;
using Airlines.Persistence.Basic.Repository;

public static class DatabaseSeeder
{
      /// <summary>
    /// Seeds the menu with data from pre-filled csv files
    /// </summary>
    /// <param name="basePath">The base path to the csv files</param>
    public static async void Seed(string? basePath = "./Airlines.Data/")
    {
        // if base path does not have a trailing slash, add one
        if (basePath?.EndsWith('/') == false)
        {
            basePath += '/';
        }

        var airportsData = FileModelParser.GetInputs(InputKey.Airport, $"{basePath}airports.csv");
        using var airportsRepo = new AirportRepository();
        var tasks = airportsData.Select(async data =>
        {
            // [Identifier, Name, City, Country, Founded, Runways]
            var entity = new Airport
            {
                Code = data[0].ToUpper(),
                Name = data[1],
                City = data[2],
                Country = data[3],
                Founded = Convert.ToUInt16(data[4]),
                Runways = Convert.ToUInt16(data[5]),
            };

            var addedIdx = await airportsRepo.AddAsync(entity);

            var msg = addedIdx != -1 ? $"[{addedIdx}] Added Airport: {entity.Name}({entity.Code})" : "Failed to add Airport";
            Console.WriteLine(msg);
        });

        await Task.WhenAll(tasks);
        Console.WriteLine("Finished seeding Airports");

        var airlinesData = FileModelParser.GetInputs(InputKey.Airline, $"{basePath}airlines.csv");
        using var airlineRepo = new AirlineRepository();
        tasks = airlinesData.Select(async data =>
        {
            // [Identifier, Name, FleetSize, Founded]
            var entity = new Airline
            {
                Code = data[0].ToUpper(),
                Name = data[1],
                Description = $"FROM CSV; SEED ON {DateTime.Now:yyyy-MM-dd HH:mm:ss}",
                FleetSize = Convert.ToUInt16(data[3]),
                Founded = Convert.ToUInt16(data[4]),
            };

            var addedIdx = await airlineRepo.AddAsync(entity);

            var msg = addedIdx != -1 ? $"[{addedIdx}] Added Airline: {entity.Name}({entity.Code})" : "Failed to add Airline";
            Console.WriteLine(msg);
        });

        await Task.WhenAll(tasks);
        Console.WriteLine("Finished seeding Airlines");

        Console.WriteLine("! Flights seeding is currently disabled !");
        // var flightsData = FileModelParser.GetInputs(InputKey.Flight, $"{basePath}flights.csv");
        // tasks = flightsData.ForEach(async data =>
        // {
        //     // [Identifier, DepartureAirport ArrivalAirport, Aircraft Model, Price, TimeInHours]
        //     var success = uint.TryParse(data[4], out var price);
        //     if (!success) throw new InvalidOperationException("Invalid price");

        //     success = double.TryParse(data[5], NumberStyles.Number, CultureInfo.InvariantCulture, out var timeInHours);
        //     if (!success) throw new InvalidOperationException($"Invalid time in hours ({timeInHours})");

        //     _ = menu.Context.FlightRepository.Add(new Flight(data[0], data[1], data[2], data[3], price, timeInHours));
        // });

        await Task.WhenAll(tasks);
        Console.WriteLine("Finished seeding Flights");

        Console.WriteLine("Finished seeding");
    }
}
