using Airlines.Business.Input;
using Airlines.Business.Structures;
using Airlines.Persistence.Basic.Repository;
using InMemory = Airlines.Persistence.Basic.Repository.InMemory;

[Obsolete("Was used to give the commands access to the repositories, but is obsolete with the new implementation. Will be removed in future version.")]
public class CommandContext
{
    public AircraftRepository AircraftRepository { get; }
    public InMemory.AirlineRepository AirlineRepository { get; }
    public RouteRepository RouteRepository { get; }
    public InMemory.AirportRepository AirportRepository { get; }
    public InMemory.FlightRepository FlightRepository { get; }

    public AirportNetwork Network { get; } = new();

    public CommandContext()
    {
        AircraftRepository = new AircraftRepository();
        AirlineRepository = new InMemory.AirlineRepository();
        RouteRepository = new RouteRepository();
        AirportRepository = new InMemory.AirportRepository();
        FlightRepository = new InMemory.FlightRepository(AircraftRepository);
    }

    public bool IsIdentifierUnique(string identifier, out InputKey? key)
    {
        key = null;
        if (AirlineRepository.Exists(identifier))
        {
            key = InputKey.Airline;
            return false;
        }

        if (FlightRepository.Exists(identifier))
        {
            key = InputKey.Flight;
            return false;
        }

        if (AirportRepository.Exists(identifier))
        {
            key = InputKey.Airport;
            return false;
        }

        return true;
    }
}
