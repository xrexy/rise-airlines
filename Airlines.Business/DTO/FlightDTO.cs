using Airlines.Persistence.Basic.Entities;
using System.ComponentModel.DataAnnotations;

namespace Airlines.Business.DTO;

public class FlightDTO
{
    [Required]
    [Display(Name = "Flight Number")]
    public string FlightNumber { get; set; } = null!;

    [Required]
    [Display(Name = "Airline")]
    public Airline Airline { get; set; } = null!;

    [Required]
    [Display(Name = "From")]
    public Airport From { get; set; }  = null!;

    [Required]
    [Display(Name = "To")]
    public Airport To { get; set; } = null!;

    [Required]
    [Display(Name = "Departure")]
    [DataType(DataType.Date)]
    [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
    [Range(typeof(DateTime), "1900-01-01", "2100-01-01")]
    public DateTime Departure { get; set; }

    [Required]
    [Display(Name = "Arrival")]
    [DataType(DataType.Date)]
    [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
    [Range(typeof(DateTime), "1900-01-01", "2100-01-01")]
    public DateTime Arrival { get; set; }
}
