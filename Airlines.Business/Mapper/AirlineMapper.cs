using Airlines.Business.DTO;
using Airlines.Persistence.Basic.Entities;

namespace Airlines.Business.Mapper;

public interface IAirlineMapper : IDtoMapper<Airline, AirlineDTO>;
public class AirlineMapper : DtoMapper<Airline, AirlineDTO>, IAirlineMapper;
