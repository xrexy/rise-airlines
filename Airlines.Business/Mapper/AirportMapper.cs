using Airlines.Business.DTO;
using Airlines.Persistence.Basic.Entities;

namespace Airlines.Business.Mapper;

public interface IAirportMapper : IDtoMapper<Airport, AirportDTO>;
public class AirportMapper : DtoMapper<Airport, AirportDTO>, IAirportMapper; 
