
using MP = AutoMapper.Mapper;
using MPConfig = AutoMapper.MapperConfiguration;

namespace Airlines.Business.Mapper;

public abstract class DtoMapper<TSource, TDestination> : IDtoMapper<TSource, TDestination>
{
    private readonly MP _mapper = new(new MPConfig(c => c.CreateMap<TSource, TDestination>().ReverseMap()));
    public TDestination Map(TSource source) => _mapper.Map<TDestination>(source);
    public TSource Map(TDestination destination) => _mapper.Map<TSource>(destination);

}
