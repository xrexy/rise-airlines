using Airlines.Business.Input;

namespace Airlines.Business.InputValidation;

public static class InputValidation
{
    public static readonly int AirlineMaxLength = 6;
    public static readonly int AirportMaxLength = 3;

    public static bool ValidateAirline(string str)
        => str.Length < AirlineMaxLength;

    public static bool ValidateAirport(string str)
        => str.Length <= AirportMaxLength && str.All(char.IsLetter);

    public static bool ValidateFlight(string str)
        => str.All(char.IsLetterOrDigit);

    public static bool Validate(InputKey key, string str)
        => key switch
        {
            InputKey.Airline => ValidateAirline(str),
            InputKey.Airport => ValidateAirport(str),
            InputKey.Flight => ValidateFlight(str),
            InputKey.Aircraft => true,
            _ => throw new InvalidInputKeyException()
        };
}
