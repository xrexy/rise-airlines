using Airlines.Business.Exceptions.Parser;
using Airlines.Business.Input;

public static class FileModelParser
{
    public static Dictionary<InputKey, uint> InputKeySize { get; } = new()
    {
        [InputKey.Airline] = 4,
        [InputKey.Airport] = 5,
        [InputKey.Flight] = 6,
        [InputKey.Aircraft] = 4
    };

    public static string[] GetPartsFromInput(string input, uint expectedLength)
    {
        var rawParts = input.Split(',');
        if (rawParts.Length != expectedLength)
        {
            throw new InvalidPartsSizeException((int)expectedLength, rawParts.Length);
        }

        return rawParts.Select(x => x.Trim()).ToArray();
    }

    /// <summary>
    /// Parses an input string to it's expected input format based on the <see cref="InputKey.Airport"/>
    /// </summary>
    /// <param name="input">The input string</param>
    /// <returns>The parsed input in format [Identifier, Name, City, Country]</returns>
    public static string[] ParseAirportFileInput(string input)
    {
        var parts = GetPartsFromInput(input, 6);

        var identifier = parts[0];
        if (identifier.Length is < 2 or > 4)
        {
            throw new InvalidParserInputException("Invalid identifier");
        }

        // name, city, country only contain letters and spaces
        if (parts.Skip(1).Take(3).Any(x => !x.All((c) => char.IsLetter(c) || c == ' ')))
        {
            throw new InvalidParserInputException("Invalid name, city or country");
        }

        // last two parts only digits
        if (parts.TakeLast(2).Any(x => !x.All((c) => char.IsDigit(c))))
        {
            throw new InvalidParserInputException("Invalid founded or runways; only digits allowed");
        }

        return parts;
    }

    /// <summary>
    /// Parses an input string to it's expected input format based on the <see cref="InputKey"/>
    /// </summary>
    /// <param name="key">The type of input to parse</param>
    /// <param name="input">The input string</param>
    /// <returns>The parsed input. Length is based on <paramref name="key"/></returns>
    public static string[] Parse(InputKey key, string input)
    {
        var size = InputKeySize.GetValueOrDefault(key);
        return key switch
        {
            InputKey.Airport => ParseAirportFileInput(input),
            InputKey.Airline => GetPartsFromInput(input, size),
            InputKey.Flight => GetPartsFromInput(input, size),
            InputKey.Aircraft => GetPartsFromInput(input, size),
            _ => throw new InvalidInputKeyException()
        };
    }

    public static List<string[]> GetInputs(InputKey key, string path, IEnumerable<string>? predefinedLines = null)
    {
        var inputs = new List<string[]>();
        var lines = predefinedLines ?? FileReader.ReadLines(path);
        foreach (var line in lines)
        {
            try
            {
                var input = Parse(key, line);
                inputs.Add(input);
            }
            catch (ArgumentException e)
            {
                Console.WriteLine($"Error: \"{e.Message}\"; Line \"{line}\"; Key \"{key}\"");
            }
        }

        return inputs;
    }
}
