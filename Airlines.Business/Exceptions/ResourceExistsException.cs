namespace Airlines.Business.Exceptions;

public class ResourceExistsException : Exception
{
    public ResourceExistsException(string message) : base(message) { }
    public ResourceExistsException() : base("Resource already exists") { }
    public ResourceExistsException(string? message, Exception? innerException) : base(message, innerException) { }
}
