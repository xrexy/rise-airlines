public class InvalidInputKeyException : InvalidOperationException
{
    public InvalidInputKeyException() : base("Invalid InputKey") { }
    public InvalidInputKeyException(string? message) : base(message) { }
    public InvalidInputKeyException(string? message, Exception? innerException) : base(message, innerException) { }
}
