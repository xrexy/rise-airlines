public class MenuInvalidSizeException : Exception
{
    public MenuInvalidSizeException() : base("Invalid menu size") { }
    public MenuInvalidSizeException(string? message) : base(message) { }
    public MenuInvalidSizeException(string? message, Exception? innerException) : base(message, innerException) { }
}
