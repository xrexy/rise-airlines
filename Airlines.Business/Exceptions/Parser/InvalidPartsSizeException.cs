namespace Airlines.Business.Exceptions.Parser;

public class InvalidPartsSizeException : ArgumentException
{
    public InvalidPartsSizeException() : base("Received invalid number of parts") { }
    public InvalidPartsSizeException(string? message) : base(message) { }
    public InvalidPartsSizeException(string? message, Exception? innerException) : base(message, innerException) { }
    public InvalidPartsSizeException(string? message, string? paramName) : base(message, paramName) { }
    public InvalidPartsSizeException(string? message, string? paramName, Exception? innerException) : base(message, paramName, innerException) { }
    public InvalidPartsSizeException(int expected, int actual) : base($"Expected {expected} parts, got {actual}") { }
}
