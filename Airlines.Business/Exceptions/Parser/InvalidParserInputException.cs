namespace Airlines.Business.Exceptions.Parser;

public class InvalidParserInputException : ArgumentException
{
    public InvalidParserInputException() : base("Invalid parser input") { }
    public InvalidParserInputException(string? message) : base(message) { }
    public InvalidParserInputException(string? message, Exception? innerException) : base(message, innerException) { }
    public InvalidParserInputException(string? message, string? paramName) : base(message, paramName) { }
    public InvalidParserInputException(string? message, string? paramName, Exception? innerException) : base(message, paramName, innerException) { }
}
