using Airlines.Business.Sorting;

namespace Airlines.Tests.SortingTests;

public class SortingTests
{
    [Fact]
    public void Sort_None()
    {
        int[] input = [4, 5, 9, 200, 23, 4, 2];
        Assert.Equal(input, Sorting.Sort(input, SortingAlgorithm.None));
    }

    [Fact]
    public void Sort_WrongAlgorithm()
        => Assert.Throws<InvalidAlgorithmException>(() => Sorting.Sort(Array.Empty<int>(), (SortingAlgorithm)9999));
}


public class AscendingSortingTests
{
    [Theory]
    [InlineData(SortingAlgorithm.BubbleSort)]
    [InlineData(SortingAlgorithm.SelectionSort)]
    public void Sort_String(SortingAlgorithm algorithm)
    {
        string[] input = ["Q", "A", "1", "1Q", "QA", "AAA"];
        string[] expected = ["1", "1Q", "A", "AAA", "Q", "QA"];

        Assert.Equal(expected, Sorting.Sort(input, algorithm, SortOrder.Ascending));
    }

    [Theory]
    [InlineData(SortingAlgorithm.BubbleSort)]
    [InlineData(SortingAlgorithm.SelectionSort)]
    public void Sort_Integer(SortingAlgorithm algorithm)
    {
        int[] input = [4, 5, 9, 200, 23, 4, 2];
        int[] expected = [2, 4, 4, 5, 9, 23, 200];
        Assert.Equal(expected, Sorting.Sort(input, algorithm, SortOrder.Ascending));
    }
}

public class DescendingSortingTests
{
    [Theory]
    [InlineData(SortingAlgorithm.BubbleSort)]
    [InlineData(SortingAlgorithm.SelectionSort)]
    public void Sort_Integer(SortingAlgorithm algorithm)
    {
        int[] input = [4, 5, 9, 200, 23, 4, 2];
        int[] expected = [200, 23, 9, 5, 4, 4, 2];
        Assert.Equal(expected, Sorting.Sort(input, algorithm, SortOrder.Descending));
    }

    [Theory]
    [InlineData(SortingAlgorithm.BubbleSort)]
    [InlineData(SortingAlgorithm.SelectionSort)]
    public void Sort_String(SortingAlgorithm algorithm)
    {
        string[] input = ["Q", "A", "1", "1Q", "QA", "AAA"];
        string[] expected = ["QA", "Q", "AAA", "A", "1Q", "1"];

        Assert.Equal(expected, Sorting.Sort(input, algorithm, SortOrder.Descending));
    }
}
