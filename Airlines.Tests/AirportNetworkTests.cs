using Airlines.Business.Structures;
using Airlines.Persistence.Basic.Models;

namespace Airlines.Tests;

public class AirportNetworkTests
{
    [Fact]
    public void AirportNetwork_CreateNode()
    {
        var network = new AirportNetwork();
        Assert.Empty(network.Nodes);

        var airport1 = new Airport("id1", "name1", "city1", "country1");
        var airport2 = new Airport("id2", "name2", "city2", "country2");
        var airport3 = new Airport("id3", "name3", "city3", "country3");
        var airport4 = new Airport("id4", "name4", "city4", "country4");

        var n1 = network.CreateNode(airport1);
        var n2 = network.CreateNode(airport2);
        var n3 = network.CreateNode(airport3);
        var n4 = network.CreateNode(airport4);

        Assert.Equal("ID1,ID2,ID3,ID4", Helpers.StringifyArray(network.Labels, ","));

        Assert.Equal(airport1, n1.Airport);

        n1.AddEdge(n2, 0, 1);
        n2.AddEdge(n3, 0, 1);
        n4.AddEdge(n1, 1, 1);

        Assert.Equal(4, network.Nodes.Count);

        Assert.Equal(2, n1.Edges.Count);
        Assert.Equal(2, n2.Edges.Count);
        _ = Assert.Single(n3.Edges);
        _ = Assert.Single(n4.Edges);

        Assert.Equal(n2, n1.Edges[0].Child);
    }

    [Fact]
    public void AirportNetwork_CreateAdjMatrix()
    {
        var network = new AirportNetwork();

        var n1 = network.CreateNode(new Airport("id1", "name1", "city1", "country1"));
        var n2 = network.CreateNode(new Airport("id2", "name2", "city2", "country2"));

        n1.AddEdge(n2, 4, 2);

        var matrix = network.CreateAdjMatrix(NetworkMatrixType.Cheapest);

        Assert.Equal(2, matrix.GetLength(0));

        Assert.Equal(0, matrix[0, 0]);
        Assert.Equal(4, matrix[0, 1]);
        Assert.Equal(4, matrix[1, 0]);
        Assert.Equal(0, matrix[1, 1]);
    }


    [Fact]
    public void AirportNetwork_AreConnected()
    {
        var network = new AirportNetwork();

        var n1 = network.CreateNode(new Airport("id1", "name1", "city1", "country1"));
        var n2 = network.CreateNode(new Airport("id2", "name2", "city2", "country2"));
        var n3 = network.CreateNode(new Airport("id3", "name3", "city3", "country3"));
        var n4 = network.CreateNode(new Airport("id4", "name4", "city4", "country4"));
        var n5 = network.CreateNode(new Airport("id5", "name5", "city5", "country5"));

        n1.AddEdge(n2, 0, 1);
        n2.AddEdge(n3, 0, 1);
        n3.AddEdge(n4, 1, 1);

        Assert.True(network.AreConnected(n1, n4));
        Assert.True(network.AreConnected(n1, n2));
        Assert.True(network.AreConnected(n1, n3));
        Assert.False(network.AreConnected(n1, n5));
        Assert.False(network.AreConnected(n3, n5));
    }

    [Fact]
    public void AirportNetwork_GetNode()
    {
        var network = new AirportNetwork();
        Assert.Null(network.GetNode("id1"));

        var airport = new Airport("id1", "name1", "city1", "country1");
        var n = network.CreateNode(airport);
        Assert.NotNull(n);
        Assert.Equal(airport, n.Airport);
    }
}
