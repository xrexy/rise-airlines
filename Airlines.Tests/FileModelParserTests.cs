
using Airlines.Business.Input;

namespace Airlines.Tests;

public class FileModelParserTests
{
    [Fact]
    public void FileModelParser_Throws()
    {
        _ = Assert.Throws<InvalidInputKeyException>(() => FileModelParser.Parse((InputKey)9999, ""));
        _ = Assert.Throws<InvalidInputKeyException>(() => FileModelParser.GetInputs((InputKey)9999, "ignored", [""]));
    }

    [Fact]
    public void FileModelParser_GetValidInputs_Airport()
    {
        string[] lines = [
          "SFO,San Francisco International Airport,San Francisco,USA,1,1",
          "SJC,San Jose International Airport,San Jose,USA,1,1",
          "SOF,Sofia Airport,Sofia,Bulgaria,1955,3",
          "TOOLONGGGGG,S@fia Airp0rt,Sof1a,Bulgaria,1",
          "SOF,S@fia Airp0rt,Sof1a,Bulgaria,asd,2",
          "invalid,what"
        ];

        var output = FileModelParser.GetInputs(InputKey.Airport, "ignored_path", lines);

        Assert.Equal(3, output.Count);

        var sofia = output[2];

        Assert.NotNull(sofia);
        Assert.Equal(6, sofia.Length);
        Assert.Equal("SOF", sofia[0]);
        Assert.Equal("Sofia Airport", sofia[1]);
        Assert.Equal("Sofia", sofia[2]);
        Assert.Equal("Bulgaria", sofia[3]);
        Assert.Equal("1955", sofia[4]);
        Assert.Equal("3", sofia[5]);
    }

    [Fact]
    public void FileModelParser_GetValidInputs_Airline()
    {
        string[] lines = [
          "AA,American Airlines,1924,1",
          "invalid",
        ];

        var output = FileModelParser.GetInputs(InputKey.Airline, "ignored_path", lines);

        _ = Assert.Single(output);

        var item = output[0];
        Assert.NotNull(item);
        Assert.Equal(4, item.Length);
        Assert.Equal("AA", item[0]);
        Assert.Equal("American Airlines", item[1]);
        Assert.Equal("1924", item[2]);
        Assert.Equal("1", item[3]);
    }
    [Fact]
    public void FileModelParser_GetValidInputs_Flight()
    {
        string[] lines = ["FL123,JFK,LAX,Boeing,5,2.4", "FL456,LAX,ORD,Aircraft,400,2.5", "invalid,length", "invalid", "nah,no,way"];

        var output = FileModelParser.GetInputs(InputKey.Flight, "ignored_path", lines);

        Assert.Equal(2, output.Count);

        var item = output[0];
        Assert.NotNull(item);
        Assert.Equal(6, item.Length);
        Assert.Equal("FL123", item[0]);
    }
}
