using Airlines.Business.Structures;
using Airlines.Persistence.Basic.Models;

namespace Airlines.Tests;

public class DijkstraTests
{
    [Fact]
    public void Dijkstra_Calculate()
    {
        var network = new AirportNetwork();

        var n0 = network.CreateNode(new Airport("id0", "name0", "city0", "country0"));
        var n1 = network.CreateNode(new Airport("id1", "name1", "city1", "country1"));
        var n2 = network.CreateNode(new Airport("id2", "name2", "city2", "country2"));
        var n3 = network.CreateNode(new Airport("id3", "name3", "city3", "country3"));
        var n4 = network.CreateNode(new Airport("id4", "name4", "city4", "country4"));

        n0.AddEdge(n1, 1, 2.5);
        n1.AddEdge(n2, 1, 3.5);
        n3.AddEdge(n4, 1, 10);

        var matrix = network.CreateAdjMatrix(NetworkMatrixType.Cheapest);
        var paths = Dijkstra.Calculate(matrix, 0, 4);
        Assert.Empty(paths);

        paths = Dijkstra.Calculate(matrix, 3, 4);
        Assert.Equal(2, paths.Count);
        Assert.Equal(3, paths[0]);
        Assert.Equal(4, paths[1]);

        paths = Dijkstra.Calculate(matrix, 0, 2);
        Assert.Equal(3, paths.Count);
        Assert.Equal(0, paths[0]);
        Assert.Equal(1, paths[1]);
        Assert.Equal(2, paths[2]);
    }
}
