namespace Airlines.Tests;

public class HelperTests
{
    [Fact]
    public void StringifyArray()
    {
        string[] input = ["a", "b", "c"];
        const string expected = "a, b, c";
        Assert.Equal(expected, Helpers.StringifyArray(input));
    }

    [Fact]
    public void StringifyArrayEmpty()
    {
        var input = Array.Empty<string>();
        const string expected = "";
        Assert.Equal(expected, Helpers.StringifyArray(input));
    }

    [Fact]
    public void StringNotEmpty()
    {
        var input = "a";
        Assert.True(Helpers.StringNotEmpty(input));

        input = null;
        Assert.False(Helpers.StringNotEmpty(input));
    }

    [Fact]
    public void ReadIntegerKey()
    {
        Assert.Equal(2, Helpers.ReadIntegerKey(predefinedKeyChar: '2'));
        Assert.Equal(8, Helpers.ReadIntegerKey(lowerBound: '7', upperBound: '8', predefinedKeyChar: '8'));

        Assert.Equal(-1, Helpers.ReadIntegerKey(predefinedKeyChar: 'c'));

        Assert.Equal(-1, Helpers.ReadIntegerKey(lowerBound: '7', upperBound: '8', predefinedKeyChar: '4'));
        Assert.Equal(-1, Helpers.ReadIntegerKey(lowerBound: '2', predefinedKeyChar: '1'));
        Assert.Equal(-1, Helpers.ReadIntegerKey(upperBound: '1', predefinedKeyChar: '2'));
    }

    [Fact]
    public void BlockUntilKeyPress()
    {
        const string text = "continue";
        var console = new MockConsole();

        console.AddKeyPress(new ConsoleKeyInfo('a', ConsoleKey.A, false, false, false));
        Helpers.BlockUntilKeyPress(console, text);

        Assert.StartsWith(text, console.Messages[0]);
    }
}
