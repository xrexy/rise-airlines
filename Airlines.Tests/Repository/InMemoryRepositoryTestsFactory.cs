using Airlines.Persistence.Basic.Repository.InMemory;

namespace Airlines.Tests.Repository;

public static class InMemoryRepositoryTestsFactory
{
    public static void Create<T>(IRepository<T> repository, Func<T> createEntity) where T : class
    {
        Assert.Equal(0, repository.Size);

        var entity = createEntity();
        Assert.True(repository.Add(entity));
        Assert.False(repository.Add(entity));
        Assert.Equal(1, repository.Size);

        Assert.True(repository.Delete(entity));
        Assert.False(repository.Delete(entity));
        Assert.Equal(0, repository.Size);

        var size = 10;
        for (var i = 0; i < size; i++)
        {
            var newEntity = createEntity();
            Assert.True(repository.Add(newEntity));
            Assert.Equal(i + 1, repository.Size);
        }

        Assert.Equal(size, repository.GetAll().Count());

        repository.DeleteAll();
        Assert.Equal(0, repository.Size);

        Assert.False(repository.Exists(entity));
        Assert.False(repository.Update(entity));
        entity = createEntity();
        Assert.True(repository.Add(entity));
        Assert.True(repository.Update(entity));
    }
}
