using Airlines.Persistence.Basic.Models;
using Airlines.Persistence.Basic.Models.AircraftTypes;
using Airlines.Persistence.Basic.Repository.InMemory;
using Base = Airlines.Persistence.Basic.Repository;

namespace Airlines.Tests.Repository;

#pragma warning disable CS0618

public class InMemoryRepositoryTests
{
    [Fact]
    public void RouteRepositoryTests()
    {
        var repository = new Base.RouteRepository();
        InMemoryRepositoryTestsFactory.Create(
          repository,
          () => new Route(GenerateRandomIdentifier(6))
        );
    }

    [Fact]
    public void AircraftRepositoryTests()
    {
        var repository = new Base.AircraftRepository();
        InMemoryRepositoryTestsFactory.Create(
          repository,
          () => new Aircraft(GenerateRandomIdentifier(6), new PrivateAircraftType(10))
        );
    }

    [Fact]
    public void AirlineRepositoryTests()
    {
        var repository = new AirlineRepository();
        InMemoryRepositoryTestsFactory.Create(
          repository,
          () => new Airline(GenerateRandomIdentifier(6), GenerateRandomIdentifier(10))
        );
    }

    [Fact]
    public void AirportRepositoryTests()
    {
        var repository = new AirportRepository();
        InMemoryRepositoryTestsFactory.Create(
          repository,
          () => new Airport(GenerateRandomIdentifier(6), GenerateRandomIdentifier(10), GenerateRandomIdentifier(10), GenerateRandomIdentifier(10))
        );
    }

    [Fact]
    public void FlightRepositoryTests()
    {
        var aircraftRepository = new Base.AircraftRepository();
        var repository = new FlightRepository(aircraftRepository);
        InMemoryRepositoryTestsFactory.Create(
          repository,
          () =>
          {
              var aircraft = new Aircraft(GenerateRandomIdentifier(6), new PrivateAircraftType(10));
              Assert.True(aircraftRepository.Add(aircraft));

              return new Flight(
                  GenerateRandomIdentifier(6),
                  GenerateRandomIdentifier(10),
                  GenerateRandomIdentifier(10),
                  aircraft.ModelName,
                  10,
                  2.4
              );
          }
        );
    }

    private static string GenerateRandomIdentifier(int length)
    {
        var random = new Random();
        var array = new char[length];

        for (var i = 0; i < length; i++)
        {
            // generate a random number between 0 and 25 and convert it to ASCII A=65 and Z=90
            array[i] = Convert.ToChar(Convert.ToInt32(Math.Floor((26 * random.NextDouble()) + 65)));
        }

        return new string(array);
    }
}

#pragma warning restore
