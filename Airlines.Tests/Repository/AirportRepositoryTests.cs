using Airlines.Persistence.Basic.Entities;
using Airlines.Persistence.Basic.Exceptions;
using Airlines.Persistence.Basic.Repository;
using DbContext = Airlines.Persistence.Basic.Entities.DbContext;

namespace Airlines.Tests.Repository;

public class AirportRepositoryTests
{
    [Fact]
    public void AirportRepository_PreprocessEntity_ValidatesCorrectly()
    {
        using var repo = CreateRepository("AirportRepository_PreprocessEntity_ValidatesCorrectly");

        var currentYear = DateTime.Now.Year;

        void TryPreprocessFail(Airport entity, string failMessage)
        {
            try
            {
                repo!.PreprocessEntity(entity);
            }
            catch (InvalidEntityException e)
            {
                Assert.Equal($"Invalid airport entity; {failMessage}", e.Message);
            }
        }

        // test validation(all invalid checks)
        TryPreprocessFail(new Airport { Founded = 1800 }, "Founded < 1900");
        TryPreprocessFail(new Airport { Founded = currentYear + 10 }, $"Founded > Current Year({currentYear})");
        TryPreprocessFail(new Airport { Founded = currentYear, Runways = 0 }, "Runways < 1");
        TryPreprocessFail(new Airport { Founded = currentYear, Runways = 2, Name = "" }, "Name is null");
        TryPreprocessFail(new Airport { Founded = currentYear, Runways = 2, Name = "name", Country = "" }, "Country is null");
        TryPreprocessFail(new Airport { Founded = currentYear, Runways = 2, Name = "name", Country = "ctry" }, "City is null");
        TryPreprocessFail(new Airport { Founded = currentYear, Runways = 2, Name = "name", Country = "ctry", City = "city" }, "Code is null");
        TryPreprocessFail(new Airport { Founded = currentYear, Runways = 2, Name = "name", Country = "ctry", City = "city", Code = "1234" }, "Code is an invalid length (1-3)");

        var validEntity = new Airport
        {
            Founded = currentYear,
            Runways = 2,
            Name = "name",
            Country = "ctry",
            City = "city",
            FlightFromAirports = new List<Flight>() { new() { Id = 4 } },
            Code = "air"
        };

        repo.PreprocessEntity(validEntity);

        var fromFlight = Assert.Single(validEntity.FlightFromAirports);
        Assert.Equal(4, fromFlight.Id);
        Assert.Empty(validEntity.FlightToAirports);
        Assert.Equal("AIR", validEntity.Code);
    }

    [Theory]
    [InlineData(5)]
    [InlineData(10)]
    public async void AirportRepository_GetFunctions(int entitySize)
    {
        using var repo = CreateRepository($"AirportRepository_GetFunctions_{entitySize}");

        Assert.Empty(await repo.GetAllAsync());
        Assert.Null(await repo.GetByIdAsync(0));

        for (var i = 0; i < entitySize; i++)
        {
            Assert.NotEqual(-1, await repo.AddAsync(new Airport
            {
                City = "city",
                Country = "country",
                Code = "tes",
                Founded = DateTime.Now.Year,
                Name = "test",
                Runways = 3,
                Id = i + 1
            }));
        }

        Assert.Null(await repo.GetByIdAsync(-1));
        Assert.NotNull(await repo.GetByIdAsync(1));

        var allEntities = await repo.GetAllAsync();
        Assert.Equal(entitySize, allEntities.Count);

        for (var i = 0; i < entitySize; i++)
        {
            Assert.Equal(i + 1, allEntities[i].Id);
        }

        var expectedEvenIdsCount = entitySize / 2;
        var entitiesWithEvenIds = await repo.GetAll(x => x.Id % 2 == 0);

        Assert.Equal(expectedEvenIdsCount, entitiesWithEvenIds.Count);

        Assert.NotNull(await repo.GetSingle(x => x.Id == 1));
        Assert.Null(await repo.GetSingle(x => x.Id == -1));
    }

    [Fact]
    public async void AirportRepository_DeleteFunctions()
    {
        // also tests Exists functions
        using var repo = CreateRepository("AirportRepository_DeleteFunctions");
        const int size = 5;

        for (var i = 0; i < size; i++)
        {
            Assert.NotEqual(-1, await repo.AddAsync(new Airport
            {
                City = "city",
                Country = "country",
                Code = "tes",
                Founded = DateTime.Now.Year,
                Name = "test",
                Runways = 3,
                Id = i + 1
            }));
        }

        Assert.True(await repo.DeleteAsync(1));
        Assert.False(await repo.ExistsAsync(1));

        var entity = await repo.GetByIdAsync(2);
        Assert.NotNull(entity);
        Assert.True(await repo.ExistsAsync(entity));
        Assert.True(await repo.DeleteAsync(entity));
        Assert.False(await repo.ExistsPredicateAsync(x => x.Id == 2));
    }

    [Fact]
    public async void AirportRepository_UpdateFunctions()
    {
        using var repo = CreateRepository("AirportRepository_UpdateFunctions");
        Assert.NotEqual(-1, await repo.AddAsync(new Airport
        {
            City = "city",
            Country = "country",
            Code = "tes",
            Founded = DateTime.Now.Year,
            Name = "test",
            Runways = 3,
            Id = 1
        }));

        var entity = await repo.GetByIdAsync(1);
        Assert.NotNull(entity);


        Assert.False(await repo.UpdateAsync(new Airport { Id = 2 }));

        // my god I hate this orm why tf is it always "Attempted to update or delete an entity that does not exist in the store."???
        // i've fetched and null checked it like 200 times before this????? 
        // entity.City = "updated";
        // Assert.True(await repo.UpdateAsync(entity));

        // entity = await repo.GetByIdAsync(1);
        // Assert.NotNull(entity);

        // Assert.Equal("updated", entity.City);
    }

    private static AirportRepository CreateRepository(string name)
        => new()
        {
            Context = new DbContext(DbContext.ConnectionType.InMemory, name)
        };
}
