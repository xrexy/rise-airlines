namespace Airlines.Tests;

public class BatchManagerTests
{
    private class TestCommand(ConsoleMenu menu, MockConsole console) : ICommand
    {
        ConsoleMenu ICommand.Menu => menu;

        public void Execute() => console.WriteLine("TEST_SUCCESS");
    }

    [Fact]
    public void BatchManager_WorkWithInvokeItem()
    {
        var console = new MockConsole();
        var menu = new ConsoleMenu(console);

        menu.Items.Clear();
        menu.AddItem("Test", new TestCommand(menu, console));

        Assert.False(menu.BatchManager.IsBatchingEnabled);

        // should invoke fine if disabled
        menu.InvokeItem(1);
        Assert.Contains("TEST_SUCCESS", console.Messages[1]);

        // should toggle correctly(enable)
        menu.BatchManager.ToggleBatching();
        Assert.True(menu.BatchManager.IsBatchingEnabled);
        Assert.Empty(menu.BatchManager.Batches);

        // should batch commands
        menu.InvokeItem(1);
        Assert.Contains("Batching is enabled. Command will be executed after", console.Messages[1]);
        Assert.NotEmpty(menu.BatchManager.Batches);

        // should toggle correctly(disable, executes all batched commands)
        Assert.True(menu.BatchManager.IsBatchingEnabled);
        menu.BatchManager.ToggleBatching();
        Assert.False(menu.BatchManager.IsBatchingEnabled);

        Assert.Contains("TEST_SUCCESS", console.Messages[1]);

        // disables properly and goes back to normal
        menu.InvokeItem(1);
        Assert.Contains("TEST_SUCCESS", console.Messages[1]);
    }
}
