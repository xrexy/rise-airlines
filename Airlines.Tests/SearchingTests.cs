using Airlines.Business.Searching;

namespace Airlines.Tests;

public class SearchingTests
{
    [Theory]
    [InlineData(SearchAlgorithm.Linear)]
    [InlineData(SearchAlgorithm.Binary)]
    public void Search_FindsCorrectIndex(SearchAlgorithm algorithm)
    {
        string[] input = ["a", "target", "b", "c"];
        Assert.Equal(1, Searching.Search(algorithm, input, "target"));
    }

    [Theory]
    [InlineData(SearchAlgorithm.Linear)]
    [InlineData(SearchAlgorithm.Binary)]
    public void Search_NotFound(SearchAlgorithm algorithm)
    {
        string[] input = ["a", "b", "c"];
        Assert.Equal(-1, Searching.Search(algorithm, input, "target"));
    }

    [Theory]
    [InlineData(SearchAlgorithm.Linear)]
    [InlineData(SearchAlgorithm.Binary)]
    public void Search_Empty(SearchAlgorithm algorithm)
    {
        string[] input = [];
        Assert.Equal(-1, Searching.Search(algorithm, input, "target"));
    }

    [Fact]
    public void Search_Throws()
        => Assert.Throws<InvalidAlgorithmException>(() => Searching.Search((SearchAlgorithm)999, [], ""));
}
