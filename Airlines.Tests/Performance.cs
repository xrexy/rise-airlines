using Airlines.Business.Sorting;
using System.Diagnostics;
using Xunit.Abstractions;

namespace Airlines.Tests;

/// <summary>
/// to see output run with: dotnet test -l "console;verbosity=detailed"
/// </summary>
public class PerformaceTests(ITestOutputHelper testOutputHelper)
{
    private readonly ITestOutputHelper _output = testOutputHelper;

    private static int[] GenerateRandomArray(int size)
    {
        var random = new Random();
        var array = new int[size];
        for (var i = 0; i < size; i++)
        {
            array[i] = random.Next(1, 1000);
        }
        return array;
    }

    [Theory(Skip = "Unneeded anymore")]
    [InlineData(1000, 50)]
    public void BubbleSortIterationsTimed(int timeMs, int arraySize)
    {
        var iterations = 0;
        var input = GenerateRandomArray(arraySize);

        Stopwatch stopwatch = new();

        stopwatch.Start();
        while (stopwatch.ElapsedMilliseconds < timeMs)
        {
            _ = Sorting.BubbleSort(input);
            iterations++;
        }

        stopwatch.Stop();

        _output.WriteLine($"Iterations: {iterations}");

        Assert.True(iterations > 0);
    }

    [Theory(Skip = "Unneeded anymore")]
    [InlineData(1000, 50)]
    [InlineData(1000, 500)]
    public void SelectionSortIterationsTimed(int timeMs, int arraySize)
    {
        var iterations = 0;
        var input = GenerateRandomArray(arraySize);

        Stopwatch stopwatch = new();

        stopwatch.Start();
        while (stopwatch.ElapsedMilliseconds < timeMs)
        {
            _ = Sorting.SelectionSort(input);
            iterations++;
        }

        stopwatch.Stop();

        _output.WriteLine($"Iterations: {iterations}");

        Assert.True(iterations > 0);
    }

    [Theory(Skip = "Unneeded anymore")]
    [InlineData(1000)]
    public void BubbleSortSpeed(int arrSize)
    {
        var input = GenerateRandomArray(arrSize);

        var stopwatch = new Stopwatch();
        stopwatch.Start();
        _ = Sorting.BubbleSort(input);
        stopwatch.Stop();
    }

    [Theory(Skip = "Unneeded anymore")]
    [InlineData(1000)]
    public void SelectionSortSpeed(int arrSize)
    {
        var input = GenerateRandomArray(arrSize);

        var stopwatch = new Stopwatch();
        stopwatch.Start();
        _ = Sorting.SelectionSort(input);
        stopwatch.Stop();

        _output.WriteLine($"True time: {stopwatch.ElapsedMilliseconds} milliseconds");
    }

    [Theory(Skip = "Unneeded anymore")]
    [InlineData(1000)]
    [InlineData(10000)]
    public void FastestSortingAlgorithm(int arrSize)
    {
        var input = GenerateRandomArray(arrSize);

        var stopwatch = new Stopwatch();
        stopwatch.Start();
        _ = Sorting.BubbleSort(input);
        stopwatch.Stop();

        var bubbleSortTime = stopwatch.ElapsedMilliseconds;

        stopwatch = new Stopwatch();
        stopwatch.Start();
        _ = Sorting.SelectionSort(input);
        stopwatch.Stop();

        var selectionSortTime = stopwatch.ElapsedMilliseconds;

        _output.WriteLine($"Bubble sort time: {bubbleSortTime} milliseconds");
        _output.WriteLine($"Selection sort time: {selectionSortTime} milliseconds");
    }
}
