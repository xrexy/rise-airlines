public class MockConsole : IConsole
{
    private readonly Queue<string> _inputs = new();
    public List<string> Messages { get; } = [];
    public List<ConsoleKeyInfo> KeyPresses { get; } = [];

    public void Write(string message) => Messages.Add(message);

    public void WriteLine(string message) => Messages.Add(message + Environment.NewLine);

    public void Clear()
    {
        Messages.Clear();
        KeyPresses.Clear();
    }

    public string? ReadLine()
    {
        if (_inputs.Count != 0)
        {
            return _inputs.Dequeue();
        }

        return null;
    }

    public ConsoleKeyInfo ReadKey()
    {
        if (KeyPresses.Count != 0)
        {
            var keyPress = KeyPresses[0];
            KeyPresses.RemoveAt(0);
            return keyPress;
        }

        // Return a default key info if none are queued
        return new ConsoleKeyInfo();
    }

    /// <summary>
    /// Use this method to enqueue inputs that ReadLine will return
    /// </summary>
    /// <param name="input"></param>
    public void AddInput(string input) => _inputs.Enqueue(input);

    /// <summary>
    /// Use this method to add a key press that ReadKey will return
    /// </summary>
    /// <param name="keyPress"></param>
    public void AddKeyPress(ConsoleKeyInfo keyPress) => KeyPresses.Add(keyPress);
}
