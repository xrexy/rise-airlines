
using Airlines.Business.DTO;
using Airlines.Business.Mapper;
using Airlines.Persistence.Basic.Repository;
using Microsoft.AspNetCore.Mvc;

[ApiController]
[Route("/api/airport")]
public class AirportApiController(IAirportRepository repository, IAirportMapper mapper) : ControllerBase
{
    [HttpGet("all")]
    public IActionResult All()
      => this.Ok(repository.GetAllAsync().Result.ConvertAll(mapper.Map));

    [HttpGet("code/{code}")]
    public IActionResult GetByAirportNumber(string code)
    {
        var airport = repository.GetSingle(x => x.Code.ToLower().Equals(code.ToLower())).Result;
        if (airport == null) return this.BadRequest();

        return this.Ok(mapper.Map(airport));
    }

    [HttpGet("{id}")]
    public IActionResult GetById(int id)
    {
        var airport = repository.GetSingle(x => x.Id == id).Result;
        if (airport == null) return this.BadRequest();

        return this.Ok(mapper.Map(airport));
    }

    [HttpPost("add")]
    public IActionResult Add([FromBody] AirportDTO airport)
    {
        if (!ModelState.IsValid) return this.BadRequest(ModelState);

        var entity = mapper.Map(airport);
        var res = repository.AddAsync(entity).Result;

        if(res == -1) return this.StatusCode(500);
        return this.Ok(new { Id = res }); // returns the id of the new airport
    }

    [HttpPut("update")]
    public IActionResult Update([FromBody] AirportDTO airport)
    {
        if (!ModelState.IsValid) return this.BadRequest(ModelState);

        var entity = mapper.Map(airport);
        var isSuccess = repository.UpdateAsync(entity).Result;

        if (!isSuccess) return this.StatusCode(500);
        return this.Ok();
    }

    [HttpDelete("delete/{id}")]
    public IActionResult Delete(int id)
    {
        var isSuccess = repository.DeleteAsync(id).Result;
        if (!isSuccess) return this.StatusCode(500);
        return this.Ok();
    }
}
