
using Airlines.Business.DTO;
using Airlines.Business.Mapper;
using Airlines.Persistence.Basic.Repository;
using Microsoft.AspNetCore.Mvc;

[ApiController]
[Route("/api/flight")]
public class FlightApiController(IFlightRepository repository, IFlightMapper mapper) : ControllerBase
{
    [HttpGet("all")]
    public IActionResult All()
      => this.Ok(repository.GetAllAsync().Result.ConvertAll(mapper.Map));

    [HttpGet("flightnumber/{flightNumber}")]
    public IActionResult GetByFlightNumber(string flightNumber)
    {
        var flight = repository.GetSingle(x => x.FlightNumber.ToLower().Equals(flightNumber.ToLower())).Result;
        if (flight == null) return this.BadRequest();

        return this.Ok(mapper.Map(flight));
    }

    [HttpGet("{id}")]
    public IActionResult GetById(int id)
    {
        var flight = repository.GetSingle(x => x.Id == id).Result;
        if (flight == null) return this.BadRequest();

        return this.Ok(mapper.Map(flight));
    }

    [HttpPost("add")]
    public IActionResult Add([FromBody] FlightDTO flight)
    {
        if (!ModelState.IsValid) return this.BadRequest(ModelState);

        var entity = mapper.Map(flight);
        var res = repository.AddAsync(entity).Result;

        if(res == -1) return this.StatusCode(500);
        return this.Ok(new { Id = res }); // returns the id of the new flight
    }

    [HttpPut("update")]
    public IActionResult Update([FromBody] FlightDTO flight)
    {
        if (!ModelState.IsValid) return this.BadRequest(ModelState);

        var entity = mapper.Map(flight);
        var isSuccess = repository.UpdateAsync(entity).Result;

        if (!isSuccess) return this.StatusCode(500);
        return this.Ok();
    }

    [HttpDelete("delete/{id}")]
    public IActionResult Delete(int id)
    {
        var isSuccess = repository.DeleteAsync(id).Result;
        if (!isSuccess) return this.StatusCode(500);
        return this.Ok();
    }
}
