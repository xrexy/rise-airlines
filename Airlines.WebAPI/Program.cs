using Airlines.Persistence.Basic.Entities;
using Airlines.Persistence.Basic.Repository;
using Airlines.Business.Mapper;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddControllers();

builder.Services.AddDbContext<DbContext>();

builder.Services.AddScoped<IFlightRepository, FlightRepository>();
builder.Services.AddScoped<IAirportRepository, AirportRepository>();
builder.Services.AddScoped<IAirlineRepository, AirlineRepository>();

builder.Services.AddScoped<IAirportMapper, AirportMapper>();
builder.Services.AddScoped<IAirlineMapper, AirlineMapper>();
builder.Services.AddScoped<IFlightMapper, FlightMapper>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    _ = app.UseSwagger();
    _ = app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.MapControllers();

app.Run();
