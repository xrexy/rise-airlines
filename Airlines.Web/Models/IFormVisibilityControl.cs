namespace Airlines.Web.Models;

public interface IFormVisibilityControl {
  public bool ShowForm { get; set; }
}
