document.addEventListener("DOMContentLoaded", () => {
  const formElement = document.querySelector(".form-container");
  if(!formElement) {
    console.error("useToggleForm requires an element with a .form-container class");
    return
  }

  const buttonElement = document.querySelector("#form-toggle-btn");
  if(!buttonElement) {
    console.error("useToggleForm requires an element with a #form-toggle-btn id");
    return
  }

  const form = useToggleableForm(formElement, buttonElement);
  form.init();
})

/**
 * Hooks to "form-toggle-btn" and toggles the first "form-container" in the page
 * 
 * @param {HTMLFormElement} form
 * @param {HTMLButtonElement} toggleBtn
 */
function useToggleableForm(form, toggleBtn) {
  const urlParams = new URLSearchParams(window.location.search);
  const showFormParamLabel = "showForm";

  function setHidden(hidden) {
    form.classList.toggle("hidden", hidden);

    urlParams.set(showFormParamLabel, !hidden);
    window.history.replaceState({}, "", `${window.location.pathname}?${urlParams.toString()}`);
  }

  function toggle() {
    setHidden(!form.classList.contains("hidden"));
  }

  function init(defaultHidden = true) {
    toggleBtn.addEventListener("click", toggle);

    setHidden(urlParams.get("showForm") ?? defaultHidden);
  }

  return {
    init,
    toggle,
    setHidden
  }
}
