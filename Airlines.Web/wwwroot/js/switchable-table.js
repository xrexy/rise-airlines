document.addEventListener("DOMContentLoaded", () => {
  const tableElement = document.querySelector("table.switchable");
  if (!tableElement) {
    console.error("switchable-table requires a table with class switchable");
    return
  }

  const button = document.querySelector("button#form-switch-btn");
  if (!button) {
    console.error("switchable-table requires an element with a #form-switch-btn id");
    return
  }

  let labels = tableElement.getAttribute("data-labels");
  if (!labels) {
    console.error("expandable-table found an element, but it does not have a data-labels attribute; should be exactly 2 values separated by a comma");
    return
  }

  labels = labels.split(",");
  if (labels.length !== 2) {
    console.error("expandable-table provided an invalid data-labels attribute; should be exactly 2 values separated by a comma");
    return;
  }

  const switchWith = tableElement.getAttribute("data-switch-with");
  if (!switchWith) {
    console.error("expandable-table found an element, but it does not have a data-switch-with attribute");
    return
  }

  const otherElement = document.querySelector(switchWith);
  if (!otherElement) {
    console.error(`expandable-table provided an invalid data-switch-with attribute(${switchWith} match anything)`);
    return;
  }

  const table = useSwitchableTable(tableElement, button, otherElement, labels);
  table.init()
})

/**
 * Toggles between a table and another specified element
 * 
 * @param {HTMLTableElement} tableElement
 * @param {HTMLButtonElement} button
 * @param {HTMLElement} otherElement
 * @param {[string, string]} labels
 */
function useSwitchableTable(tableElement, button, otherElement, labels) {
  // 0 = tableElement
  // 1 = otherElement
  let current = 0;
  function toggle() {
    current = (current + 1) % 2;
    tableElement.classList.toggle("hidden");
    otherElement.classList.toggle("hidden");
    button.innerHTML = labels[current];
  }

  function init(toggleOnInit = false) {
    button.innerHTML = labels[0];
    otherElement.classList.add("hidden");

    button.addEventListener("click", toggle);

    if(toggleOnInit) toggle();
  }

  return { init, toggle }
}
