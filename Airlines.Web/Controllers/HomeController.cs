using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Airlines.Web.Models;
using Airlines.Persistence.Basic.Repository;

namespace Airlines.Web.Controllers;

public class HomeController(IAirportRepository airportRepository, IAirlineRepository airlineRepository, IFlightRepository flightRepository) : Controller
{
    public IActionResult Index()
    {
        // TODO: All controllers(home, airline, airport, flight etc) are fetching same data seperately
        // Ideally they should share some sort of store, so we share data and not do duplicate fetches everywhere
        HomeViewModel model = new()
        {
            AirlinesCount = airlineRepository.GetAllAsync().Result.Count,
            AirportsCount = airportRepository.GetAllAsync().Result.Count,
            FlightsCount = flightRepository.GetAllAsync().Result.Count
        };
        return this.View(model);
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error() => this.View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
}
