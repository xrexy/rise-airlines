
public class ConsoleWrapper : IConsole
{
    public void Clear() => Console.Clear();
    public ConsoleKeyInfo ReadKey() => Console.ReadKey();
    public string? ReadLine() => Console.ReadLine();
    public void Write(string message) => Console.Write(message);
    public void WriteLine(string message) => Console.WriteLine(message);
}
