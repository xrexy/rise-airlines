public class SeeBatchingStatusCommand(ConsoleMenu menu) : IUnbatchableCommand
{
    ConsoleMenu ICommand.Menu => menu;

    public void Execute()
    {
        var status = menu.BatchManager.IsBatchingEnabled switch
        {
            true => "ENABLED",
            false => "DISABLED",
        };

        menu.PrintItems(menu.CurrentItem!.SubItems, $"Batching is {status}");
    }
}
