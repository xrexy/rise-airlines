public class ResetBatchingStatusCommand(ConsoleMenu menu) : IUnbatchableCommand
{
    ConsoleMenu ICommand.Menu => menu;

    public void Execute()
    {
        if(!menu.BatchManager.IsBatchingEnabled)
        {
            menu.PrintItems(menu.CurrentItem!.SubItems, "Batching is not enabled");
            return;
        }

        var response = menu.PromptUntilValid(
            "Toggle? (Y/N): ",
            "You are about to clear your batched commands. This will also disable batching. Are you sure?",
            (str) => str.ToUpper() is "Y" or "N"
        ).ToUpper();

        if (response == "Y")
        {
            var count = menu.BatchManager.Batches.Count;
            menu.BatchManager.Batches.Clear();
            menu.BatchManager.DisableBatching();

            menu.PrintItems(menu.CurrentItem!.SubItems, $"Cleared {count} command(s) and disabled batching");
        }
        else
        {
            menu.PrintItems(menu.CurrentItem!.SubItems);
        }
    }
}
