[Obsolete("Flight entities DON'T have aircraft attributes")]
public class CargoReservationCommand(ConsoleMenu menu) : ICommand
{
    ConsoleMenu ICommand.Menu => menu;

    public void Execute() { }

    // public void Execute()
    // {
    //     var flightIdentifier = menu.PromptUntilValidIdentifier(promptPrefix: "Flight ", upperLength: 5, allowDigits: true);
    //     var context = menu.Context;

    //     var flight = context.FlightRepository.GetById(flightIdentifier);
    //     if (flight == null)
    //     {
    //         menu.PrintItems(menu.CurrentItem!.SubItems, $"\"{flightIdentifier}\" is not a valid flight identifier");
    //         return;
    //     }

    //     // can't be null, since it's validated when creating the flight
    //     var aircraft = context.AircraftRepository.GetByFlight(flight);
    //     if (aircraft.Type is not ICargoAircraftType aircraftType)
    //     {
    //         menu.PrintItems(menu.CurrentItem!.SubItems, $"\"{flightIdentifier}\" doesn't support cargo");
    //         return;
    //     }

    //     var cargoWeight = menu.PromptUntilValidUint("Cargo Weight: ", "Must be a number", maximum: 10_000);
    //     if (flight.WeightUsed + cargoWeight > aircraftType.CargoWeight)
    //     {
    //         menu.PrintItems(menu.CurrentItem!.SubItems, $"Flight: \"{flightIdentifier}\" doesn't have enough cargo capacity");
    //         return;
    //     }

    //     var cargoVolume = menu.PromptUntilValidDouble("Cargo Volume: ", "Must be a number", maximum: 10_000);
    //     if (flight.VolumeUsed + cargoVolume > aircraftType.CargoVolume)
    //     {
    //         menu.PrintItems(menu.CurrentItem!.SubItems, $"Flight: \"{flightIdentifier}\" doesn't have enough cargo volume");
    //         return;
    //     }

    //     flight.WeightUsed += cargoWeight;
    //     flight.VolumeUsed += cargoVolume;

    //     menu.PrintItems(menu.CurrentItem!.SubItems, $"Reserved {cargoWeight}kg. and {cargoVolume} volume cargo for flight {flightIdentifier}");
    // }
}
