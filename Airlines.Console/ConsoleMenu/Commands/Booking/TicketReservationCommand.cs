[Obsolete("Flight entities DON'T have aircraft attributes")]
public class TicketReservationCommand(ConsoleMenu menu) : ICommand
{
    ConsoleMenu ICommand.Menu => menu;

    public void Execute() { }

    // public void Execute()
    // {
    //     var flightIdentifier = menu.PromptUntilValidCode(promptPrefix: "Flight ", upperLength: 5, allowDigits: true);
    //     var context = menu.Context;

    //     var flight = context.FlightRepository.GetById(flightIdentifier);
    //     if (flight == null)
    //     {
    //         menu.PrintItems(menu.CurrentItem!.SubItems, $"\"{flightIdentifier}\" is not a valid flight identifier");
    //         return;
    //     }

    //     // can't be null, since it's validated when creating the flight
    //     var aircraft = context.AircraftRepository.GetByFlight(flight)!;
    //     if (aircraft.Type is not (ITicketableAircraftType ticketableAircraftType and ICargoAircraftType cargoAircraftType))
    //     {
    //         menu.PrintItems(menu.CurrentItem!.SubItems, $"\"{flightIdentifier}\" isn't supported");
    //         return;
    //     }

    //     var availableSeats = ticketableAircraftType.Seats - flight.SeatsUsed;
    //     var seats = menu.PromptUntilValidUint("Seats", $"Must be a number (0-{availableSeats})", maximum: (int)availableSeats);

    //     var weightLeft = cargoAircraftType.CargoWeight - flight.WeightUsed;
    //     var volumeLeft = cargoAircraftType.CargoVolume - flight.VolumeUsed;

    //     short CalculateMax(uint maximumWeight, double maximumVolume)
    //     {
    //         var baggagesWeightLeft = weightLeft / maximumWeight;
    //         var baggagesVolumeLeft = volumeLeft / maximumVolume;
    //         return Convert.ToInt16(Math.Min(baggagesWeightLeft, baggagesVolumeLeft));
    //     }

    //     var smallBaggageMaximum = CalculateMax(SmallBaggage.MaximumWeight, SmallBaggage.MaximumVolume);
    //     var smallBaggageCount = menu.PromptUntilValidUint(
    //         "Small Baggage",
    //         $"Must be a number (0-{smallBaggageMaximum})",
    //         maximum: smallBaggageMaximum
    //     );
    //     var weightUsed = smallBaggageCount * SmallBaggage.MaximumWeight;
    //     var volumeUsed = smallBaggageCount * SmallBaggage.MaximumVolume;

    //     var largeBaggageMaximum = CalculateMax(LargeBaggage.MaximumWeight, LargeBaggage.MaximumVolume);
    //     var largeBaggageCount = menu.PromptUntilValidUint(
    //         "Large Baggage",
    //         $"Must be a number (0-{largeBaggageMaximum})",
    //         maximum: largeBaggageMaximum
    //     );
    //     weightUsed += largeBaggageCount * LargeBaggage.MaximumWeight;
    //     volumeUsed += largeBaggageCount * LargeBaggage.MaximumVolume;

    //     flight.WeightUsed += weightUsed;
    //     flight.VolumeUsed += volumeUsed;
    //     flight.SeatsUsed += seats;

    //     menu.PrintItems(menu.CurrentItem!.SubItems, $"Reserved {weightUsed}kg. and {volumeUsed} volume baggage for flight {flightIdentifier}");
    // }
}
