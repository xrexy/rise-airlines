public interface ICommand
{
  protected ConsoleMenu Menu { get; }
  public void Execute();
}
