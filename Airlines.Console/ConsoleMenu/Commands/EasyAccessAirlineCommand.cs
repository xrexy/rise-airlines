using Airlines.Persistence.Basic.Repository;

public class EasyAccessAirlineCommand(ConsoleMenu menu) : ICommand
{
    ConsoleMenu ICommand.Menu => menu;

    public async void Execute()
    {
        using var repo = new AirlineRepository();

        var code = menu.PromptUntilValidCode();
        var airline = await repo.GetSingle(x => x.Code.Equals(code, StringComparison.CurrentCultureIgnoreCase));
        var message = airline == null ? "Airline not found" : $"Valid Airline: {airline.Name} [Id: {airline.Id}]";

        menu.PrintItems(menu.Items, message);
    }
}
