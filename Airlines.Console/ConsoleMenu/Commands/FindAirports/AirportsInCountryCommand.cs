using Airlines.Persistence.Basic.Repository;

public class AirportsInCountryCommand(ConsoleMenu menu) : ICommand
{
    ConsoleMenu ICommand.Menu => menu;

    public async Task<int> Proccess()
    {
        using var repo = new AirportRepository();
        var country = menu.PromptUntilValid(
            "Country: ",
            "Only letters are allowed.",
            (str) => str.All(c => c == ' ' || char.IsLetter(c)) && str.Length is >= 1 and <= 64
        ).ToUpper();

        var airports = await repo.GetAll(x => x.Country.Equals(country, StringComparison.CurrentCultureIgnoreCase));
        if (airports.Count != 0)
        {
            menu.PrintHeader();
            menu.Console.WriteLine(Helpers.BuildNumberedFormatList(
                 airports,
                 (airport) => airport.Name
             ));

            return 0;
        }

        menu.GoToInitalPage();
        menu.PrintItems(menu.Items, $"No Airports found in \"{country}\"");
        return -1;
    }

    public async void Execute()
    {
        _ = await this.Proccess();
        Helpers.BlockUntilKeyPress(menu.Console);
        menu.PrintItems(menu.Items);
    }
}
