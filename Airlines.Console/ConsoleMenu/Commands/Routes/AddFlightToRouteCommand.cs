using Airlines.Persistence.Basic.Repository;
using Models = Airlines.Persistence.Basic.Models;

public class AddFlightToRouteCommand(ConsoleMenu menu) : ICommand
{
    ConsoleMenu ICommand.Menu => menu;

    public async void Execute()
    {
        using var flightRepo = new FlightRepository();
        var routeCode = menu.PromptUntilValidCode("Route ").ToUpper();

        var route = menu.Routes.GetById(routeCode);
        if (route == null)
        {
            menu.PrintItems(menu.CurrentItem!.SubItems, $"\"{routeCode}\" does not exist");
            return;
        }

        var flightCode = menu.PromptUntilValidCode("Flight ", upperLength: 5, allowDigits: true).ToUpper();
        var flight = await flightRepo.GetSingle(x => x.FlightNumber.Equals(flightCode, StringComparison.CurrentCultureIgnoreCase));
        if (flight == null)
        {
            menu.PrintItems(menu.CurrentItem!.SubItems, $"Flight \"{flightCode}\" does not exist");
            return;
        }

        route.AddFlight(new Models.Flight(
            flight.FlightNumber,
            flight.FromAirport.Code,
            flight.ToAirport.Code,

            // flight entity doesn't have these so using arbitrary values
            "N/A",
            10,
            10
        ));
        menu.PrintItems(menu.CurrentItem!.SubItems, $"Added Flight: \"{flight.FlightNumber}\" to Route: \"{route.Identifier}\"");
    }
}
