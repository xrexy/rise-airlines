using Airlines.Business.Structures;
using System.Text;

public class ShortestRoutePathCommand(ConsoleMenu menu) : ICommand
{
    ConsoleMenu ICommand.Menu => menu;

    internal string PrintPath(ref double?[,] graph, string[] labels, string src, string dest)
    {
        var source = Array.IndexOf(labels, src);
        var destination = Array.IndexOf(labels, dest);

        if (source == -1 || destination == -1)
        {
            return "Invalid Airport Identifier";
        }

        var sb = new StringBuilder($"Shortest path from {src} to {dest} is:");

        var paths = Dijkstra.Calculate(graph, source, destination);

        if (paths.Count > 0)
        {
            double? path_length = 0;
            for (var i = 0; i < paths.Count - 1; i++)
            {
                var length = graph[paths[i], paths[i + 1]];
                path_length += length;
                _ = sb.Append($" {labels[paths[i]]} -> ");
            }
            _ = sb.AppendLine($"{labels[destination]}");
        }
        else
        {
            _ = sb.AppendLine("No Path");
        }

        return sb.ToString();
    }

    public void Execute()
    {
        var type = menu.PromptUntilValidUint("Type: ", "0=Cheapest, 1=Fewest Stops", 0, 1);
        var src = menu.PromptUntilValidCode("Source ").ToUpper();
        var dest = menu.PromptUntilValidCode("Destination ").ToUpper();

        var network = menu.Network;
        var matrix = network.CreateAdjMatrix((NetworkMatrixType)type);
        menu.PrintItems(menu.CurrentItem!.SubItems, this.PrintPath(ref matrix, network.Labels, src, dest));
    }
}
