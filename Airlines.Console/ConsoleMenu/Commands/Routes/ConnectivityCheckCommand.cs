using Airlines.Business.Structures;

public class ConnectivityCheckCommand(ConsoleMenu menu) : ICommand
{
    ConsoleMenu ICommand.Menu => menu;

    internal AirportNetwork.Node? FindNode(string identifier)
        => menu.Network.Nodes.Find(node => node.Airport.Identifier == identifier);

    public void Execute()
    {
        var src = menu.PromptUntilValidCode("Source ").ToUpper();
        var sourceNode = this.FindNode(src);
        if (sourceNode == null)
        {
            menu.PrintItems(menu.CurrentItem!.SubItems, $"\"{src}\" does not exist");
            return;
        }

        var dest = menu.PromptUntilValidCode("Destination ").ToUpper();
        var destinationNode = this.FindNode(dest);
        if (destinationNode == null)
        {
            menu.PrintItems(menu.CurrentItem!.SubItems, $"\"{dest}\" does not exist");
            return;
        }

        var areConnected = menu.Network.AreConnected(sourceNode, destinationNode);
        menu.PrintItems(menu.CurrentItem!.SubItems, string.Format("{0} is {1}connected to {2}", src, areConnected ? "" : "not ", dest));
    }
}
