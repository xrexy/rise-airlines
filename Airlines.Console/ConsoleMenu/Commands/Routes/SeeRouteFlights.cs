public class SeeRouteFlightsCommand(ConsoleMenu menu) : ICommand
{
    ConsoleMenu ICommand.Menu => menu;

    public int Process()
    {
        var identifier = menu.PromptUntilValidCode().ToUpper();
        var route = menu.Routes.GetById(identifier);
        if (route == null)
        {
            menu.PrintItems(menu.CurrentItem!.SubItems, $"\"{identifier}\" does not exist");
            return -1;
        }

        if (route.Flights.Count == 0)
        {
            menu.PrintItems(menu.CurrentItem!.SubItems, $"\"{identifier}\" has no flights");
            return -1;
        }

        var message = Helpers.BuildNumberedFormatList(
            route.Flights,
            (flight) => $"{flight.Identifier}: {flight.DepartureAirport} -> {flight.ArrivalAirport}"
        );
        menu.PrintItems(menu.CurrentItem!.SubItems, message);

        return 0;
    }

    public void Execute()
    {
        if (this.Process() == 0)
        {
            Helpers.BlockUntilKeyPress(menu.Console);
            menu.PrintItems(menu.CurrentItem!.SubItems);
        }
    }
}
