public class RemoveLastFlightFromRouteCommand(ConsoleMenu menu) : ICommand
{
    ConsoleMenu ICommand.Menu => menu;

    public void Execute()
    {
        var identifier = menu.PromptUntilValidCode().ToUpper();
        var route = menu.Routes.GetById(identifier);
        if (route == null)
        {
            menu.PrintItems(menu.CurrentItem!.SubItems, $"\"{identifier}\" does not exist");
            return;
        }

        if (route.Flights.Count == 0)
        {
            menu.PrintItems(menu.CurrentItem!.SubItems, $"\"{identifier}\" has no flights");
            return;
        }

        route.RemoveLastFlight();

        menu.PrintItems(menu.CurrentItem!.SubItems, $"Removed last flight from Route: \"{route.Identifier}\"");
    }
}
