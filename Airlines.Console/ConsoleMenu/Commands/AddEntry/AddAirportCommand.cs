using Airlines.Persistence.Basic.Entities;
using Airlines.Persistence.Basic.Repository;

public class AddAirportCommand(ConsoleMenu menu) : ICommand
{
    ConsoleMenu ICommand.Menu => menu;

    public async void Execute()
    {
        using var repo = new AirportRepository();
        var name = menu.PromptUntilValidName();
        var code = menu.PromptUntilValidCode();
        var founded = menu.PromptUntilValidUint("Founded year: ", "Must be after 1900", minimum: 1900, maximum: DateTime.Now.Year);
        var runways = menu.PromptUntilValidUint("Runways: ", "Must be at least one", minimum: 1);

        var isDuplicate = await repo.ExistsPredicateAsync(x => x.Code == code.ToUpper());
        if (isDuplicate)
        {
            menu.PrintItems(menu.CurrentItem!.SubItems, $"An airport with code \"{code}\" already exists.");
            return;
        }

        var city = menu.PromptUntilValid(
            "City: ",
            "Must be at least 4 characters",
            (str) => str.All(c => c == ' ' || char.IsLetterOrDigit(c)) && str.Length is >= 4 and <= 32
        );

        var country = menu.PromptUntilValid(
            "Country: ",
            "Must be at least 4 characters",
            (str) => str.All(c => c == ' ' || char.IsLetterOrDigit(c)) && str.Length is >= 4 and <= 32
        );

        var airport = new Airport
        {
            Name = name,
            Code = code,
            City = city,
            Country = country,
            Founded = (int)founded,
            Runways = (int)runways
        };

        var addedIdx = await repo.AddAsync(airport);
        var success = addedIdx != -1;

        var message = success ? $"[{addedIdx}] Added Airport: {airport.Name}({airport.Code})" : "Failed to add airport";
        menu.PrintItems(menu.CurrentItem!.SubItems, message);
    }
}
