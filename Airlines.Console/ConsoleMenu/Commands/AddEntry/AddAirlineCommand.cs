using Airlines.Persistence.Basic.Entities;
using Airlines.Persistence.Basic.Repository;

public class AddAirlineCommand(ConsoleMenu menu) : ICommand
{
    ConsoleMenu ICommand.Menu => menu;

    public async void Execute()
    {
        using var repo = new AirlineRepository();
        var name = menu.PromptUntilValidName();
        var code = menu.PromptUntilValidCode(lowerLength: 2, upperLength: 2);
        var founded = menu.PromptUntilValidUint("Founded year: ", "Must be after 1900", minimum: 1900, maximum: DateTime.Now.Year);
        var fleetSize = menu.PromptUntilValidUint("Fleet Size: ", "Must be at least one", minimum: 1);

        var isDuplicate = await repo.ExistsPredicateAsync(x => x.Code == code.ToUpper());
        if (isDuplicate)
        {
            menu.PrintItems(menu.CurrentItem!.SubItems, $"An airline with code \"{code}\" already exists.");
            return;
        }

        var airline = new Airline
        {
            Code = code,
            Name = name,
            Founded = (int)founded,
            FleetSize = (int)fleetSize,
        };
        var addedIdx = await repo.AddAsync(airline);
        var success = addedIdx != -1;

        if (!success) return;

        menu.PrintItems(menu.CurrentItem!.SubItems, $"[{addedIdx}] Added Airline: {airline.Name}({airline.Code})");
    }
}
