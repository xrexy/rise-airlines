﻿public static class Program
{
    private static void Main(string[] _)
    {
        var menu = new ConsoleMenu(new ConsoleWrapper());
        menu.PopulateInMemoryObjects();

        // Print the start page
        menu.PrintItems(menu.Items);

        // validate if the menu size is in range 1-9
        // 0 is reserved for "Exit" and "Go Back" internal actions
        var menuSize = menu.Size;
        if (menuSize == 0)
        {
            throw new MenuInvalidSizeException("No menu items found. Use .AddMenuItem() to add menu items.");
        }

        if (menuSize > 9)
        {
            throw new MenuInvalidSizeException("Menu size cannot be greater than 9. Consider using subitems instead.");
        }

        // transform the menu size to a single character and start the main loop
        while (true)
        {
            var currentItem = menu.CurrentItem;

            var idx = Helpers.ReadIntegerKey(upperBound: (currentItem?.SubItems.Count ?? menuSize).ToString()[0]);
            menu.InvokeItem(idx);
        }
    }
}
