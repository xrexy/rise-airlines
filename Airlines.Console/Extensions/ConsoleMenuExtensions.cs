using Airlines.Business.Input;
using Models = Airlines.Persistence.Basic.Models;
using Airlines.Persistence.Basic.Repository;
using Airlines.Persistence.Basic.Models.AircraftTypes;
using Airlines.Business.Exceptions.Parser;

[assembly: System.Runtime.CompilerServices.InternalsVisibleTo("Airlines.Tests")]
public static class ConsoleMenuExtensions
{
    public static void PopulateInMemoryObjects(this ConsoleMenu menu, string basePath = "./Airlines.Data/")
    {
        // if base path does not have a trailing slash, add one
        if (basePath?.EndsWith('/') == false)
        {
            basePath += '/';
        }

        PopulateAircraftData(menu, FileModelParser.GetInputs(InputKey.Aircraft, $"{basePath}aircrafts.csv"));
        PopulateAirportNetwork(menu);
    }

    internal static void PopulateAircraftData(ConsoleMenu menu, List<string[]> lines)
    {
        var expectedSize = FileModelParser.InputKeySize[InputKey.Aircraft];
        lines.ForEach(data =>
        {
            if(data.Length != expectedSize)
            {
                throw new InvalidPartsSizeException((int)expectedSize, data.Length);
            }

            // [Model, ...type args]
            IAircraftType type;
            // The absence of Cargo Load, Cargo Volume, or Seats helps determine the aircraft type.
            // Cargo Aircraft:
            // Boeing 747-8F, 140000, 854.5, -
            // Passenger Aircraft:
            // Airbus A320, 20000, 37.4, 150
            // Private Aircraft:
            // Gulfstream G650, -, -, 18
            if (!data.Any(x => x.Contains('-')))
            {
                type = new PassengerAircraftType(Convert.ToUInt16(data[3]), Convert.ToUInt32(data[1]), Convert.ToDouble(data[2].Replace('.', ',')));
            }
            else if (data[3].Contains('-'))
            {
                type = new CargoAircraftType(Convert.ToUInt32(data[1]), Convert.ToDouble(data[2].Replace('.', ',')));
            }
            else if (data[1].Contains('-') && data[2].Contains('-'))
            {
                type = new PrivateAircraftType(Convert.ToUInt16(data[3]));
            }
            else
            {
                throw new InvalidOperationException("Invalid aircraft type");
            }

            _ = menu.Aircrafts.Add(new Models.Aircraft(data[0], type));
        });
    }

    public static async void PopulateAirportNetwork(this ConsoleMenu menu)
    {
        var network = menu.Network;
        using var airportsRepo = new AirportRepository();
        using var flightsRepo = new FlightRepository();

        var airports = await airportsRepo.GetAllAsync();
        foreach (var airport in airports)
        {
            var n = network.CreateNode(new Models.Airport(airport.Code, airport.Name, airport.City, airport.Country));
        }

        var flights = await flightsRepo.GetAllAsync();
        foreach (var flight in flights)
        {
            var departureNode = network.Nodes.Find(x => x.Airport.Identifier == flight.FromAirport.Code);
            if (departureNode == null) continue;

            var arrivalNode = network.Nodes.Find(x => x.Airport.Identifier == flight.ToAirport.Code);
            if (arrivalNode == null) continue;

            // flights entities don't have price according to the database requirements
            // also don't have time in hours and there's no way to currently calculate it.
            // that's why we are using an arbitrary price and time in hours
            departureNode.AddEdge(arrivalNode, 10, 10);
        }
    }
}
